/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
 */

package com.phonegap.calendariopolarpilsen;

import android.os.Bundle;
import android.os.Environment;

import java.io.File;

import org.apache.cordova.*;

public class CalendarioPilsen extends CordovaActivity
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // Set by <content src="index.html" /> in config.xml
        loadUrl(launchUrl);
		File folder;
		File folder2;
		/*
		 * File path = Environment.getExternalStoragePublicDirectory(
		 * Environment.DIRECTORY_PICTURES ); //this throws error in Android
		 * 2.2
		 */
		
		folder2 = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
		folder = new File(folder2, "PILSEN");
	
		if(!folder.exists()) {
			folder.mkdirs();
		}
    }
}
