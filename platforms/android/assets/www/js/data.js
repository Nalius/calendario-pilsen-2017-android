var events = {};
var gaPlugin;
window.localStorage.setItem("event_CLICK",0);
var event_CLICK = 0;
var analytics;
t = new Date(),
today = ((t.getMonth() + 1) < 10 ? '0' + (t.getMonth() + 1) : (t.getMonth() + 1)) + '-' + (t.getDate() < 10 ? '0' + t.getDate() : t.getDate()) + '-' +t.getFullYear();
events[today] = [{content: 'TODAY', allDay: true, recordatorio:0}];
document.addEventListener("deviceready", onDeviceReady, false);
window.appRootDirName = "PILSEN";
function onDeviceReady() {
    cordova.plugins.notification.local.on("click", function (id, state, json) {
      window.localStorage.setItem("event_CLICK",1);
      console.log(JSON.stringify(id));
      navigator.notification.alert('Evento: "'+id.title+'"', function(){}, 'Eventos', 'OK');
    });

    window.localStorage.setItem("edicion", 0);
    var db = window.openDatabase("test", "1.0", "Test DB", 1000000);
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

    function populateDB(tx) {
        tx.executeSql('CREATE TABLE IF NOT EXISTS DEMO (ID INTEGER PRIMARY KEY, dia_inicio VARCHAR, mes_inicio VARCHAR, ano_inicio VARCHAR,hora_inicio VARCHAR,minutos_inicio VARCHAR,dia_fin VARCHAR, mes_fin VARCHAR, ano_fin VARCHAR,hora_fin VARCHAR,minutos_fin VARCHAR,titulo VARCHAR,descripcion VARCHAR,repeticion VARCHAR,recordatorio VARCHAR,chica VARCHAR)');
        tx.executeSql('CREATE TABLE IF NOT EXISTS DEMO2 (ID INTEGER PRIMARY KEY, chica VARCHAR, tipoCalendario VARCHAR, imagen VARCHAR, fechaNacimiento VARCHAR)');
        tx.executeSql('CREATE TABLE IF NOT EXISTS ALARMAS (ID INTEGER PRIMARY KEY, IDA VARCHAR, TITULO VARCHAR, CHICA VARCHAR, REPETICION VARCHAR, HORA VARCHAR, MINUTOS VARCHAR, FORMATO VARCHAR, TOGGLE VARCHAR)');
    }

    function errorCB(err) {
        //alert("Error processing SQL: "+err.code);
    }

    function successCB() {
        db.transaction(queryDates, errorCB);
    }

    
    function queryDates(tx){
        tx.executeSql ('SELECT * FROM DEMO', [], successDate, errorCB);
    }
    function queryConfig(tx){
        tx.executeSql ('SELECT * FROM DEMO2 WHERE ID=1', [], successConfig, errorCB);
    }
    function successDate(tx, results) {
        events = {};
        if(results.rows.length > 0){
            for (var i = 0; i < results.rows.length; i++) {
                //console.log ("ID=" + results.rows.item(i).ID+" Fecha Inicio= "+results.rows.item(i).mes_inicio+"-"+results.rows.item(i).dia_inicio+"-"+results.rows.item(i).ano_inicio);
                var hora_inicio =results.rows.item(i).hora_inicio;
                var minutos_inicio = results.rows.item(i).minutos_inicio;
                var hora_fin = results.rows.item(i).hora_fin;
                var minutos_fin = results.rows.item(i).minutos_fin;
                var mes_inicio = results.rows.item(i).mes_inicio;
                var dia_inicio = results.rows.item(i).dia_inicio;
                var ano_inicio = results.rows.item(i).ano_inicio;
                var ano_fin = results.rows.item(i).ano_fin;
                var dia_fin = results.rows.item(i).dia_fin;
                var mes_fin = results.rows.item(i).mes_fin;
                var startDate = (mes_inicio < 10 ? '0' + (parseInt(mes_inicio)+1 ) : (parseInt(mes_inicio)+1)) + "-" + ((dia_inicio) < 10 ? '0' + (dia_inicio) : (dia_inicio)) + "-" + ano_inicio;
                var endDate = mes_fin+"-"+dia_fin+"-"+ano_fin;
                var dtstart_calendario = hora_inicio+":"+minutos_inicio;
                var dtend_calendario = hora_fin+":"+minutos_fin;
                var title = results.rows.item(i).titulo;
                var ID=results.rows.item(i).ID;
                var last_evento_ID = parseInt(ID) + 1;
                window.localStorage.setItem("last_evento_ID", last_evento_ID);
                var repeticion = results.rows.item(i).repeticion;
                //console.log ("ID=" + results.rows.item(i).ID+" Fecha Inicio= "+startDate);
                if((startDate in events)){
                    events[startDate].push({content: title, startTime: dtstart_calendario, endTime: dtend_calendario, note: ID, recordatorio:repeticion});
                }else{
                    events[startDate]=[{content: title, startTime: dtstart_calendario, endTime: dtend_calendario, note: ID, recordatorio:repeticion}];
                }
            }
        }else{
            window.localStorage.setItem("last_evento_ID", 555);
        }
        db.transaction(queryConfig, errorCB);
    }
    function successConfig(tx, results) {
        if (results.rows.length > 0) {
            for (var i = 0; i < results.rows.length; i++) {
                var chica = results.rows.item(i).chica;
                var tipoCalendario = results.rows.item(i).tipoCalendario;
                var imagen = results.rows.item(i).imagen;
                var ID=results.rows.item(i).ID;
                window.localStorage.setItem("Calendario",tipoCalendario);
                window.localStorage.setItem("chicaSeleccionada",chica);
                window.localStorage.setItem("fotoSeleccion",imagen);
            };
            window.wakeuptimer.consulta( successConsulta, errorConsulta,{});
            window.localStorage.setItem("registrado",1);
        }else{
            location.href = "#/main";
        }
    }

    function error (argument) {
        //alert("error:"+argument);
    }
    //The directory to store data
    function fail() {
        //alert("failed to get filesystem");
    }

    function gotFS(fileSystem) {
        //alert("filesystem got");
        window.fileSystem = fileSystem;
        fileSystem.root.getDirectory(window.appRootDirName, {
            create : true,
            exclusive : false
        }, dirReady, fail);
    }

    function dirReady(entry) {
        window.appRootDir = entry;
    }

    window.plugins.NativeAudio.preloadSimple( 'Grabiela', 'alarmas/Grabiela.mp3', function(msg){
    }, function(msg){
        //console.log( 'error: ' + msg );
    });

    window.plugins.NativeAudio.preloadSimple( 'Karen', 'alarmas/Karen.mp3', function(msg){
    }, function(msg){
        //console.log( 'error: ' + msg );
    });
    analytics = navigator.analytics;
    analytics.setTrackingId('UA-57584978-12');
    analytics.sendAppView('Home', successHandler, errorHandler);
    var Fields    = analytics.Fields,
     HitTypes  = analytics.HitTypes,
     LogLevel  = analytics.LogLevel,
     params    = {};
    params[Fields.HIT_TYPE]     = HitTypes.APP_VIEW;
    params[Fields.SCREEN_NAME]  = 'Home';
    analytics.setLogLevel(LogLevel.INFO);
    analytics.send(params, function(){db.transaction(populateDB, errorCB, successCB);}, errorHandler);
}



function successConsulta(evento){
    if(evento.type == "wakeup"){
        //console.log("wilfrido1:"+evento.message);
        if(evento.chica == "Grabiela"){
            window.plugins.NativeAudio.loop( 'Grabiela' );
        }else if(evento.chica == "Karen"){
            window.plugins.NativeAudio.loop( 'Karen' );
        }
        window.localStorage.setItem("tituloAlarma", evento.titulo);
        window.localStorage.setItem("HoraAlarma", evento.hora);
        window.localStorage.setItem("MinutosAlarma", evento.minutos);
        window.localStorage.setItem("FormatoAlarma", evento.formato);
        location.href = "#/alarma";
    }else if(evento){
        var obj = JSON.parse(evento);
        //console.log("wilfrido2:"+obj.message);

        if(obj.chica == "Grabiela"){
            window.plugins.NativeAudio.loop( 'Grabiela' );
        }else if(obj.chica == "Karen"){
            window.plugins.NativeAudio.loop( 'Karen' );
        }
        window.localStorage.setItem("tituloAlarma", obj.titulo);
        window.localStorage.setItem("HoraAlarma", obj.hora);
        window.localStorage.setItem("MinutosAlarma", obj.minutos);
        window.localStorage.setItem("FormatoAlarma", obj.formato);
        location.href = "#/alarma";
    }else{
        //console.log("es nulo");
        location.href = "#/tab/home";
    }
}
function errorConsulta(evento){
    //console.log(evento);
}


function successHandler(){
	
}
function errorHandler(){
	
}
function successDespertador(evento){
    if(evento == "OK"){
        alert("Alarma Creada");
        window.plugins.NativeAudio.preloadSimple( 'Grabiela', 'alarmas/Grabiela.mp3', function(msg){
        }, function(msg){
         //console.log( 'error: ' + msg );
        });

        window.plugins.NativeAudio.preloadSimple( 'Karen', 'alarmas/Karen.mp3', function(msg){
        }, function(msg){
         //console.log( 'error: ' + msg );
        });
    }else if(evento.type == "wakeup"){
        //console.log("wilfrido1:"+evento.message);
        var obj = JSON.parse(evento.extra);
        if(obj.chica == "Grabiela"){
            window.plugins.NativeAudio.loop( 'Grabiela' );
        }else if(obj.chica == "Karen"){
            window.plugins.NativeAudio.loop( 'Karen' );
        }
        window.localStorage.setItem("tituloAlarma", obj.titulo);
        window.localStorage.setItem("HoraAlarma", obj.hora);
        window.localStorage.setItem("MinutosAlarma", obj.minutos);
        window.localStorage.setItem("FormatoAlarma", obj.formato);
        location.href = "#/alarma";
    }
}
function successDespertador2(evento){
    if(evento == "OK"){
        window.plugins.NativeAudio.preloadSimple( 'Grabiela', 'alarmas/Grabiela.mp3', function(msg){
        }, function(msg){
         //console.log( 'error: ' + msg );
        });

        window.plugins.NativeAudio.preloadSimple( 'Karen', 'alarmas/Karen.mp3', function(msg){
        }, function(msg){
         //console.log( 'error: ' + msg );
        });
    }else if(evento.type == "wakeup"){
        //console.log("wilfrido1:"+evento.message);
        var obj = JSON.parse(evento.extra);
        if(obj.chica == "Grabiela"){
            window.plugins.NativeAudio.loop( 'Grabiela' );
        }else if(obj.chica == "Karen"){
            window.plugins.NativeAudio.loop( 'Karen' );
        }
        window.localStorage.setItem("tituloAlarma", obj.titulo);
        window.localStorage.setItem("HoraAlarma", obj.hora);
        window.localStorage.setItem("MinutosAlarma", obj.minutos);
        window.localStorage.setItem("FormatoAlarma", obj.formato);
        location.href = "#/alarma";
    }
}
function errorDespertador(evento){
  //console.log(evento);
}
function realarmaSuccess(evento){
    //alert("Alarma Creada");
    //console.log("success");
}
function realarmaError(evento){
    //console.log(evento);
}