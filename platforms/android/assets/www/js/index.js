/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var startDate = "";
var endDate = "";
var hora_inicio = "";
var minutos_inicio = "";
var hora_fin = "";
var minutos_fin = "";
var title = "My nice event";
var eventLocation = "Home";
var notes = "Some notes about this event.";
var carga_calendario = 0;
var $jq = $.noConflict();
var dia = "";
var dia_semana = ["Dom","Lun","Mar","Mie","Jue","Vie","Sab"];
var meses = ["2017","enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre"];
var db = window.openDatabase("test", "1.0", "Test DB", 1000000);

angular.module('starter', ['ionic', 'ionic-datepicker','ionic-timepicker'])

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

  $ionicConfigProvider.tabs.position('bottom');
    var chica = 0;
    $stateProvider
    .state('loader', {
    url: '/',
    templateUrl: 'templates/loader.html',
    controller: 'loaderCTRL'
    })
    .state('main', {
    url: '/main',
    templateUrl: 'templates/main.html',
    controller: 'MainCtrl'
    })
    .state('tipoCalendario', {
    url: '/tipoCalendario',
    templateUrl: 'templates/tipoCalendario.html',
    controller: 'tipoCalendarioCtrl'
    })
    .state('seleccion_chica', {
    url: '/seleccion_chica',
    templateUrl: 'templates/seleccion_chica.html',
    controller: 'seleccion_chicaCtrl'
    })
    .state('slider', {
    url: '/slider',
    templateUrl: 'templates/slider.html',
    controller: 'sliderCtrl'
    })
    .state('alarma', {
    url: '/alarma',
    templateUrl: 'templates/despertadorAlarma.html',
    controller: 'AlarmaHomeTabCtrl'
    })
    .state('tabs', {
      url: "/tab",
      abstract: true,
      templateUrl: "templates/tabs.html",
      controller: 'TabCtrl'
    })
    .state('tabs.home', {
      url: "/home",
      views: {
        'home-tab': {
          templateUrl: "templates/calendario_home.html",
          controller: 'HomeTabCtrl'
        }
      }
    })
    .state('tabs.despertador', {
      url: "/despertador",
      views: {
        'despertador-tab': {
          templateUrl: "templates/despertador_home.html",
          controller: "despertadorHomeTabCtrl"
        }
      }
    })
    .state('tabs.bodega', {
      url: "/bodega",
      views: {
        'bodega-tab': {
          templateUrl: "templates/bodega_home.html",
          controller: 'bodegaHomeTabCtrl'
        }
      }
    })
    .state('tabs.bodega.karen',{
      url:"/karen",
      views: {
        'inception': {
          templateUrl: "templates/karen.html",
          controller: "ControllerKaren"
        }
      }
    })
    .state('tabs.settings', {
      url: "/settings",
      views: {
        'settings-tab': {
          templateUrl: "templates/settings_home.html",
          controller: 'settingsHomeTabCtrl'
        }
      }
    })
    .state('tabs.calendario_meses', {
      url: "/calendario_meses",
      views: {
        'home-tab': {
          templateUrl: "templates/calendario_meses.html",
          controller: 'calendario_mesesCtrl'
        }
      }
    })
    switch(chica) {
    case 0:
        ////alert("chica 1");
        break;
    case 1:
        ////alert("chica 2");
        break;
    }
    $urlRouterProvider.otherwise("/");

})




.controller('MainCtrl', function($scope, $state) {
  $scope.$on('$ionicView.enter', function(){
    analytics.sendAppView("/Registro", function(){},function(){});
  })
  $scope.toSeleccion = function(){
    $state.go('tipoCalendario');
  };
  $scope.dateMayorEdad = new Date("Wed Jan 23 1991 00:00:00");
  $scope.diaMayorEdad = "23";
  $scope.mesMayorEdad = "09";
  $scope.anoMayorEdad = "1991";
  window.localStorage.setItem("dateMayorEdad", $scope.dateMayorEdad);
  window.localStorage.setItem("diaMayorEdad", $scope.diaMayorEdad);
  window.localStorage.setItem("mesMayorEdad", $scope.mesMayorEdad);
  window.localStorage.setItem("anoMayorEdad", $scope.anoMayorEdad);
  var options = {
    date: new Date("Wed Jan 23 1991 00:00:00"),
    mode: 'date', // or 'time'
    minDate: '-631065660000',
    maxDate : '915148740000',
    allowOldDates: true,
    allowFutureDates: true,
    doneButtonLabel: 'DONE',
    doneButtonColor: '#F2F3F4',
    cancelButtonLabel: 'CANCEL',
    cancelButtonColor: '#000000'
  };

  $scope.mayorEdad = function(){
    datePicker.show(options, function(date){
      //console.log(date);
      $scope.dateMayorEdad = new Date(date);
      $scope.diaMayorEdad = date.getDate();
      $scope.mesMayorEdad = parseInt(date.getMonth())+1;
      $scope.anoMayorEdad = date.getFullYear();
    }
    , function(error){
      //console.log(error);
    });

  }
  $scope.datepickerObject = {
      titleLabel: '',  //Optional
      todayLabel: false,  //Optional
      closeLabel: 'Cancelar',  //Optional
      setLabel: 'Aceptar',  //Optional
      setButtonType : 'button-positive',  //Optional
      todayButtonType : 'button-positive',  //Optional
      closeButtonType : 'button-positive',  //Optional
      inputDate: new Date(1991, 8, 23),  //Optional
      mondayFirst: true,  //Optional
      templateType: 'popup', //Optional
      showTodayButton: 'true', //Optional
      modalHeaderColor: 'bar-positive', //Optional
      modalFooterColor: 'bar-positive', //Optional
      from: new Date(1950, 1, 1), //Optional
      to: new Date(1998, 1, 1),  //Optional
      callback: function (val) {  //Mandatory
        if (val) {
        $scope.dateMayorEdad = new Date(val);
        $scope.diaMayorEdad = val.getDate();
        $scope.mesMayorEdad = parseInt(val.getMonth())+1;
        $scope.anoMayorEdad = val.getFullYear();
        $scope.datepickerObject.inputDate = new Date(val);

        };

      },
      dateFormat: 'dd-MM-yyyy', //Optional
      closeOnSelect: false, //Optional
    };

})
.controller('tipoCalendarioCtrl', function($scope, $state) {
 
  $scope.$on('$ionicView.enter', function(){
    analytics.sendAppView("/TipoCalendario", function(){},function(){});
  })
  
  window.localStorage.setItem("Calendario",1);
  $scope.edicion = window.localStorage.getItem("edicion");
  $scope.tipoCalendario = function(index) {
    $scope.tipoCalendarioSeleccion = index;
    $jq("body .loader_seleccion").show();
    window.localStorage.setItem("Calendario", index);
    if(index == 1){
      if($scope.edicion == 1){
        db.transaction(dbAniversario1, errorCB1, successCB1);
        function dbAniversario1(tx) {
            tx.executeSql('UPDATE DEMO2 SET chica=2,tipoCalendario=1,imagen=4,fechaNacimiento="" WHERE ID=1');
            //tx.executeSql('INSERT INTO DEMO (ID, dia_inicio, mes_inicio, ano_inicio,hora_inicio,minutos_inicio,dia_fin,mes_fin,ano_fin,hora_fin,minutos_fin,titulo,descripcion,repeticion,recordatorio,chica) VALUES (null, 6,1,2016,22,30,7,1,2016,22,30,"prueba","prueba","daily",10,2)');
        }

        function errorCB1(err) {
            //alert("Error processing SQL: "+err.code);
        }

        function successCB1() {
            //alert("success!");
            $state.go('tabs.home');
        }
      }else{
        db.transaction(dbAniversario2, errorCB2, successCB2);
        function dbAniversario2(tx) {
            tx.executeSql('INSERT INTO DEMO2 (ID, chica, tipoCalendario, imagen, fechaNacimiento) VALUES (null, 2,1,4,"")');
            //tx.executeSql('INSERT INTO DEMO (ID, dia_inicio, mes_inicio, ano_inicio,hora_inicio,minutos_inicio,dia_fin,mes_fin,ano_fin,hora_fin,minutos_fin,titulo,descripcion,repeticion,recordatorio,chica) VALUES (null, 6,1,2016,22,30,7,1,2016,22,30,"prueba","prueba","daily",10,2)');
        }

        function errorCB2(err) {
            //alert("Error processing SQL: "+err.code);
        }

        function successCB2() {
            //alert("success!");
            $state.go('tabs.home');
        }
      }
      analytics.sendEvent("Tipo Calendario", "Click", "Aniversario", 1,function(){},function(){});
    }else if(index == 2){
      $state.go('seleccion_chica');
      analytics.sendEvent("Tipo Calendario", "Click", "Personalizado", 1,function(){},function(){});
    }else if(index == 3) {
        var chica = Math.floor((Math.random() * 5) + 1);
        var imagen = Math.floor((Math.random() * 5) + 1);
        if($scope.edicion == 1){
          db.transaction(dbRamdom3, errorCB3, successCB3);
          function dbRamdom3(tx) {
              tx.executeSql('UPDATE DEMO2 SET chica='+chica+',tipoCalendario=3,imagen='+imagen+',fechaNacimiento="" WHERE ID=1');
              //tx.executeSql('INSERT INTO DEMO (ID, dia_inicio, mes_inicio, ano_inicio,hora_inicio,minutos_inicio,dia_fin,mes_fin,ano_fin,hora_fin,minutos_fin,titulo,descripcion,repeticion,recordatorio,chica) VALUES (null, 6,1,2016,22,30,7,1,2016,22,30,"prueba","prueba","daily",10,2)');
          }

          function errorCB3(err) {
              //alert("Error processing SQL: "+err.code);
          }

          function successCB3() {
              //alert("update!");
              window.localStorage.setItem("chicaSeleccionada", chica);
              window.localStorage.setItem("fotoSeleccion", imagen);
              $state.go('tabs.home');
          }
        }else{
          db.transaction(dbRamdom4, errorCB4, successCB4);
          function dbRamdom4(tx) {
              tx.executeSql('INSERT INTO DEMO2 (ID, chica, tipoCalendario, imagen, fechaNacimiento) VALUES (null, '+chica+',3,'+imagen+',"")');
              //tx.executeSql('INSERT INTO DEMO (ID, dia_inicio, mes_inicio, ano_inicio,hora_inicio,minutos_inicio,dia_fin,mes_fin,ano_fin,hora_fin,minutos_fin,titulo,descripcion,repeticion,recordatorio,chica) VALUES (null, 6,1,2016,22,30,7,1,2016,22,30,"prueba","prueba","daily",10,2)');
          }

          function errorCB4(err) {
              //alert("Error processing SQL: "+err.code);
          }

          function successCB4() {
              //alert("success modafaka!");
              window.localStorage.setItem("chicaSeleccionada", chica);
              window.localStorage.setItem("fotoSeleccion", imagen);
              $state.go('tabs.home');
          }
        }
        analytics.sendEvent("Tipo Calendario", "Click", "Aleatorio", 1,function(){},function(){});
    }
    
  };

})
.controller('seleccion_chicaCtrl', function($scope, $state) {
  
  $scope.$on('$ionicView.enter', function(){
    $jq("body .loader_seleccion").hide();
    analytics.sendAppView("/SeleccionChica", function(){},function(){});
  })
 
  // Called to navigate to the main app
  $scope.toSlider = function() {
    $state.go('slider');
  };
   window.localStorage.setItem("chicaSeleccionada", 0);
  $scope.eleccionChica = function(index){
    //console.log(index);
    window.localStorage.setItem("chicaSeleccionada", index);
  }
  $jq('.chicas button').on({ 'touchstart' :function(){
    seleccion_chica = $jq(this).attr("data-chica");
    $jq(".chicas button").removeClass("active");
    $jq(".opciones_chicas button").siblings("span").removeClass("active");
    $jq(this).addClass("active");
                          }})
})

.controller('sliderCtrl', function($scope, $state, $ionicSlideBoxDelegate) {
  $scope.$on('$ionicView.enter', function(){
    analytics.sendAppView("/SliderChicas", function(){},function(){});
  })
  
  $jq('.opciones_chicas button').on({ 'touchstart' : function(){
    seleccion_chica = $jq(this).attr("data-chica");
    $jq(".opciones_chicas button").siblings("span").removeClass("active");
    $jq(this).siblings("span").addClass("active");
                                    }})
  // Called to navigate to the main app
  $scope.toPrincipal = function() {
    var chica = window.localStorage.getItem("chicaSeleccionada");
    var imagen = window.localStorage.getItem("fotoSeleccion");
    if(window.localStorage.getItem("edicion") == 1){
      db.transaction(dbPersonalizado5, errorCB5, successCB5);
      function dbPersonalizado5(tx) {
         tx.executeSql('UPDATE DEMO2 SET chica='+chica+',tipoCalendario=2, imagen='+imagen+',fechaNacimiento="" WHERE ID=1');
          //tx.executeSql('INSERT INTO DEMO (ID, dia_inicio, mes_inicio, ano_inicio,hora_inicio,minutos_inicio,dia_fin,mes_fin,ano_fin,hora_fin,minutos_fin,titulo,descripcion,repeticion,recordatorio,chica) VALUES (null, 6,1,2016,22,30,7,1,2016,22,30,"prueba","prueba","daily",10,2)');
      }

      function errorCB5(err) {
          //alert("Error processing SQL: "+err.code);
      }

      function successCB5() {
          //alert("success!");
          window.localStorage.setItem("chicaSeleccionada", chica);
          window.localStorage.setItem("fotoSeleccion", imagen);
          analytics.sendEvent("Chica Elegida", "Click", $scope.chicaSeleccion.Mychica.name, 1,function(){},function(){});
          $state.go('tabs.home');
      }
    }else{
      db.transaction(dbPersonalizado6, errorCB6, successCB6);
      function dbPersonalizado6(tx) {
          tx.executeSql('INSERT INTO DEMO2 (ID, chica, tipoCalendario, imagen, fechaNacimiento) VALUES (null, '+chica+',2,'+imagen+',"")');
          //tx.executeSql('INSERT INTO DEMO (ID, dia_inicio, mes_inicio, ano_inicio,hora_inicio,minutos_inicio,dia_fin,mes_fin,ano_fin,hora_fin,minutos_fin,titulo,descripcion,repeticion,recordatorio,chica) VALUES (null, 6,1,2016,22,30,7,1,2016,22,30,"prueba","prueba","daily",10,2)');
      }

      function errorCB6(err) {
          //alert("Error processing SQL: "+err.code);
      }

      function successCB6() {
          //alert("success update");
          window.localStorage.setItem("chicaSeleccionada", chica);
          window.localStorage.setItem("fotoSeleccion", imagen);
          analytics.sendEvent("Chica Elegida", "Click", $scope.chicaSeleccion.Mychica.name, 1,function(){},function(){});
          $state.go('tabs.home');
      }
    }
    
  };
  $scope.next = function() {
    $ionicSlideBoxDelegate.next();
  };
  $scope.previous = function() {
    $ionicSlideBoxDelegate.previous();
  };
  $scope.chicaSeleccion =[
    {name: "Karen"},
    {name: "Georgina"},
    {name: "Grabiela"},
    {name: "Gabriella"},
    {name: "Anmarie"},
    {name: "Grupal"}

  ];
  $scope.chicaSeleccionada = window.localStorage.getItem("chicaSeleccionada");
  $scope.chicaSeleccion.Mychica =  $scope.chicaSeleccion[$scope.chicaSeleccionada];
  $scope.eleccionChica = function(index){
    //console.log(index);
    window.localStorage.setItem("chicaSeleccionada", index);
    $scope.chicaSeleccionada = window.localStorage.getItem("chicaSeleccionada");
    $scope.chicaSeleccion.Mychica =  $scope.chicaSeleccion[$scope.chicaSeleccionada];
  }
  // Called each time the slide changes
  $scope.slideChanged = function(index) {
    window.localStorage.setItem("fotoSeleccion", index);
    $scope.slideIndex = index;
    //console.log("chicaSeleccionada:"+window.localStorage.getItem("chicaSeleccionada")+"fotoN:"+window.localStorage.getItem("fotoSeleccion"));
  };
  $scope.slideChanged(0);
})
.controller('TabCtrl', function($scope, $state,$ionicModal,$ionicSlideBoxDelegate,$rootScope) {
  $jq("body .loader_seleccion").hide();
  $scope.tipoCalendarioSeleccion = window.localStorage.getItem("Calendario");
  //console.log("Calendario="+window.localStorage.getItem("Calendario"));
  $scope.chicasCalendarPremium = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
  $scope.chicasCalendarPremiumDate = new Date().getMonth();
  if($scope.tipoCalendarioSeleccion == 1){
    $scope.layout = 'CalendarioPremium/'+$scope.chicasCalendarPremium[$scope.chicasCalendarPremiumDate];
  }else if($scope.tipoCalendarioSeleccion == 2 || $scope.tipoCalendarioSeleccion == 3){
    $scope.chicaSeleccion =[
      {name: "Karen"},
      {name: "Georgina"},
      {name: "Grabiela"},
      {name: "Gabriella"},
      {name: "Anmarie"},
      {name: "Grupal"}

    ];
    $scope.chicaSeleccionada = window.localStorage.getItem("chicaSeleccionada");
    $scope.fotoSeleccion = window.localStorage.getItem("fotoSeleccion");
    $scope.chicaSeleccion.Mychica =  $scope.chicaSeleccion[$scope.chicaSeleccionada];
    $scope.layout = 'DetalleChicaEscogida/'+$scope.chicaSeleccion.Mychica.name+'/EscogeChica_'+$scope.chicaSeleccion.Mychica.name+'_'+(parseInt($scope.fotoSeleccion)+1);
  }

})
.controller('HomeTabCtrl', function($scope, $state,$ionicModal,$ionicSlideBoxDelegate,$ionicPlatform,$rootScope) {
  var mes = new Date().getMonth();
  var anio = new Date().getFullYear();
  $scope.mes = meses[mes+1];

  $scope.$on('$ionicView.enter', function(){
    
    $scope.selectModalSlider = $ionicSlideBoxDelegate.$getByHandle('modalSlider');
    $scope.selectModalSlider.enableSlide(false);
    analytics.sendAppView("/Home/Calendario", function(){},function(){});
    $scope.tipoCalendarioSeleccion = window.localStorage.getItem("Calendario");
    $jq("body .loader_seleccion").hide();
    $scope.chicasCalendarPremium = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
    $scope.chicasCalendarPremiumDate = new Date().getMonth();
    if($scope.tipoCalendarioSeleccion == 1){
      $scope.layout = 'CalendarioPremium/'+$scope.chicasCalendarPremium[$scope.chicasCalendarPremiumDate];
    }else if($scope.tipoCalendarioSeleccion == 2 || $scope.tipoCalendarioSeleccion == 3){
      $scope.chicaSeleccion =[
       // {name: "Karen"},
        {name: "Georgina"},
        {name: "Grabiela"},
        {name: "Gabriella"},
        {name: "Anmarie"},
        {name: "Grupal"}

      ];
      $scope.chicaSeleccionada = window.localStorage.getItem("chicaSeleccionada");
      $scope.fotoSeleccion = window.localStorage.getItem("fotoSeleccion");
      $scope.chicaSeleccion.Mychica =  $scope.chicaSeleccion[$scope.chicaSeleccionada];
      $scope.layout = 'DetalleChicaEscogida/'+$scope.chicaSeleccion.Mychica.name+'/EscogeChica_'+$scope.chicaSeleccion.Mychica.name+'_'+(parseInt($scope.fotoSeleccion)+1);
    }
    
    $scope.barraGeneral = "Crear evento";
    $scope.model = {};


    $scope.repeticion = [
      {name:'Diario', value:"0", repeat:"day"},
      {name:'Semanal', value:"1", repeat:"week"},
      {name:'Mensual', value:"2", repeat:"month"}
    ]
    $scope.repeticion.myRepeticion = $scope.repeticion[0]; // red
    
    $scope.chicaVozEvento = [
      {name:'Karen', value:"KAREN_EVENTOS.mp3"},
      {name:'Gabriella', value:"GRABRIELA EVENTO.mp3"},
      {name:'Grabiela', value:"GABRIELA EVENTO.mp3"},
    ]
    $scope.chicaVozEvento.chicaVozEventoModel = $scope.chicaVozEvento[0];

    $scope.start_Time = formatDate(new Date());
    $scope.end_Time = formatDate(new Date());
    $scope.hours_end = new Date().getHours();
    $scope.minutes_end = new Date().getMinutes();
    $scope.hours_start = new Date().getHours();
    $scope.minutes_start = new Date().getMinutes();
    $scope.start_Date = new Date();
    $scope.start_DateCreate = new Date();
    $scope.end_DateCreate = new Date();
    dia = new Date($scope.start_Date).getDay();
    $scope.start_Date = dia_semana[dia]+" "+$scope.start_Date.getDate()+"/"+( parseInt($scope.start_Date.getMonth()) + 1)+"/"+$scope.start_Date.getFullYear();

    $scope.end_Date = new Date();
    $scope.end_Date = dia_semana[dia]+" "+$scope.end_Date.getDate()+"/"+( parseInt($scope.end_Date.getMonth()) + 1)+"/"+$scope.end_Date.getFullYear();
    $scope.model.titulo = "";
    $scope.openModal();
  })

        //console.log("App view (menu) entered.");

    //console.log($scope.tipoCalendarioSeleccion);
    $scope.myGoBack = function() {
      $scope.slideTo(0);
    };

    // Called each time the slide changes
    $scope.left_mes = function() {
      if($scope.mesControl > 1){
        $scope.mesControl = parseInt($scope.mesControl) -1;
        $scope.mes = meses[$scope.mesControl];
        $jq( '#calendar_mes' ).calendario({
          caldata : events,
          fillEmpty: false,
          month: $scope.mesControl,
          year: anio,
          displayWeekAbbr : true,
          events: ['click', 'focus']
        });
      }else{
      }

    }
    $scope.right_mes = function() {
      if($scope.mesControl < 12){
        $scope.mesControl = parseInt($scope.mesControl) +1;
        $scope.mes = meses[$scope.mesControl];
        $jq( '#calendar_mes' ).calendario({
          caldata : events,
          fillEmpty: false,
          month: $scope.mesControl,
          year: anio,
          displayWeekAbbr : true,
          events: ['click', 'focus']
        });
      }else{
      }
    }
    $scope.slideChanged = function(index) {
      $scope.slideIndex = index;
      ////console.log(index);
      if(index == 0){
        var mes = new Date().getMonth();
        $scope.mes = meses[mes+1];
      }
    };
    $scope.slideToCalendar = function(index){
      $scope.selectModalSlider.slide(index);
    }
    var i = 0;
    $scope.slideTo = function(index){
      $scope.selectModalSlider.slide(index);
      if(index == 4){
        $jq(".modal_meses .controles").hide();
        $scope.layout = "img/FONDO-PAPEL";
      }else if(index == 3 || index == 5){
        $jq(".modal_meses .controles").hide();
        $jq(".modal_meses .barra_volverGeneral").show();
        if($scope.tipoCalendarioSeleccion == 1){
          $scope.layout = 'CalendarioPremium/'+$scope.chicasCalendarPremium[$scope.chicasCalendarPremiumDate];
        }else{
          $scope.layout = 'DetalleChicaEscogida/'+$scope.chicaSeleccion.Mychica.name+'/EscogeChica_'+$scope.chicaSeleccion.Mychica.name+'_'+(parseInt($scope.fotoSeleccion)+1); 
        }
      }else{
        $jq(".modal_meses .controles").show();
        $jq(".modal_meses .barra_volverGeneral").hide();
        if($scope.tipoCalendarioSeleccion == 1){
          $scope.layout = 'CalendarioPremium/'+$scope.chicasCalendarPremium[$scope.chicasCalendarPremiumDate];
        }else{
          $scope.layout = 'DetalleChicaEscogida/'+$scope.chicaSeleccion.Mychica.name+'/EscogeChica_'+$scope.chicaSeleccion.Mychica.name+'_'+(parseInt($scope.fotoSeleccion)+1); 
        }
      }
      if(index == 5){
        $scope.barraGeneral = "Editar evento";
      }
      //console.log(index);

      if(index == 3){
        $scope.barraGeneral = "Crear evento";
        $scope.start_Time = formatDate(new Date());
        $scope.end_Time = formatDate(new Date());
        $scope.hours_end = new Date().getHours();
        $scope.minutes_end = new Date().getMinutes();
        $scope.hours_start = new Date().getHours();
        $scope.minutes_start = new Date().getMinutes();
        if($scope.start_Date && $scope.start_Date != ''){
          dia = new Date($scope.start_Date).getDay();
          $scope.start_DateCreate= new Date($scope.start_Date);
          $scope.start_Date = dia_semana[dia]+" "+$scope.start_Date.getDate()+"/"+( parseInt($scope.start_Date.getMonth()) + 1)+"/"+$scope.start_Date.getFullYear();
        }else{
          $scope.start_Date = new Date();
          dia = new Date($scope.start_Date).getDay();
          $scope.start_Date = dia_semana[dia]+" "+$scope.start_Date.getDate()+"/"+( parseInt($scope.start_Date.getMonth()) + 1)+"/"+$scope.start_Date.getFullYear();
        }
        if($scope.end_Date && $scope.end_Date != ''){
          dia = new Date($scope.end_Date).getDay();
          $scope.end_DateCreate= new Date($scope.end_Date);
          $scope.end_Date = dia_semana[dia]+" "+$scope.end_Date.getDate()+"/"+( parseInt($scope.end_Date.getMonth()) + 1)+"/"+$scope.end_Date.getFullYear();
        }else{
          $scope.end_Date = new Date();
          dia = new Date($scope.end_Date).getDay();
          $scope.end_Date = dia_semana[dia]+" "+$scope.end_Date.getDate()+"/"+( parseInt($scope.end_Date.getMonth()) + 1)+"/"+$scope.end_Date.getFullYear();
        }
        $scope.repeticion.myRepeticion = $scope.repeticion[0];
        $scope.chicaVozEvento.chicaVozEventoModel = $scope.chicaVozEvento[0];
        $scope.model.titulo = "";
      }
      if(index == 1){
        var mes_seleccionado = new Date().getMonth()+1;
        $scope.mesControl = mes_seleccionado;
        $jq( '#calendar_mes' ).calendario({
            caldata : events,
            fillEmpty: false,
            month: mes_seleccionado,
            year: anio,
            displayWeekAbbr : true,
            events: ['click', 'focus']
        });
        $scope.date = new Date();
        $scope.hora = "";
        $scope.mes = meses[mes_seleccionado];
        $jq('div.fc-row > div').on('onDayClick.calendario', function(e, dateprop) {
          if( dateprop.data.content.length > 0 ) {
            $jq(".eventos_container ul li .eventos_individuals").html("");
            $jq(".eventos_container > h1").html(dateprop.weekdayname+","+dateprop.day+" de "+dateprop.monthname+" de "+dateprop.year);
            for (var i = 0; i < dateprop.data.content.length; i++) {
                $scope.date = new Date(dateprop.data.startTime[i]);
                $jq(".eventos_container ul li").each(function(){
                    if($jq(this).attr("datetime") == $scope.date.getHours()){
                        if($scope.date.getHours() > 12){
                          $jq(this).children(".eventos_individuals").append("<time datacalndario='"+dateprop.data.note[i]+"' ><h1>"+formatDate($scope.date)+"</h1><span></span><p>"+dateprop.data.content[i]+"</p></time>");
                        }else if($scope.date.getHours() == 00){
                          $jq(this).children(".eventos_individuals").append("<time datacalndario='"+dateprop.data.note[i]+"' ><h1>"+formatDate($scope.date)+"</h1><span></span><p>"+dateprop.data.content[i]+"</p></time>");
                        } else if($scope.date.getHours() == 12){
                          $jq(this).children(".eventos_individuals").append("<time datacalndario='"+dateprop.data.note[i]+"' ><h1>"+formatDate($scope.date)+"</h1><span></span><p>"+dateprop.data.content[i]+"</p></time>");
                        }else{
                          $jq(this).children(".eventos_individuals").append("<time datacalndario='"+dateprop.data.note[i]+"' ><h1>"+formatDate($scope.date)+"</h1><span></span><p>"+dateprop.data.content[i]+"</p></time>");
                        }
                    }
                })
                
            };
            $scope.dateIndiv=(dateprop);
            $scope.slideTo(2);
          }
        });
      }
      if(index == 0){
        $jq( '#calendar1' ).calendario({
            caldata : events,
            fillEmpty: false,
            month: 01,
            year: anio,
            displayWeekAbbr : true,
            events: []
        });
        $jq( '#calendar2' ).calendario({
            caldata : events,
            fillEmpty: false,
            month: 02,
            year: anio,
            displayWeekAbbr : true,
            events: []
        });
        $jq( '#calendar3' ).calendario({
            caldata : events,
            month: 03,
            fillEmpty: false,
            year: anio,
            displayWeekAbbr : true,
            events: []
        });
        $jq( '#calendar4' ).calendario({
            caldata : events,
            month: 04,
            fillEmpty: false,
            year: anio,
            displayWeekAbbr : true,
            events: []
        });
        $jq( '#calendar5' ).calendario({
            caldata : events,
            month: 05,
            fillEmpty: false,
            year: anio,
            displayWeekAbbr : true,
            events: []
        });
        $jq( '#calendar6' ).calendario({
            caldata : events,
            month: 06,
            year: anio,
            fillEmpty: false,
            displayWeekAbbr : true,
            events: []
        });
        $jq( '#calendar7' ).calendario({
            caldata : events,
            month: 07,
            fillEmpty: false,
            year: anio,
            displayWeekAbbr : true,
            events: []
        });
        $jq( '#calendar8' ).calendario({
            caldata : events,
            month: 08,
            fillEmpty: false,
            year: anio,
            displayWeekAbbr : true,
            events: []
        });
        $jq( '#calendar9' ).calendario({
            caldata : events,
            month: 09,
            fillEmpty: false,
            year: anio,
            displayWeekAbbr : true,
            events: []
        });
        $jq( '#calendar10' ).calendario({
            caldata : events,
            month: 10,
            fillEmpty: false,
            year: anio,
            displayWeekAbbr : true,
            events: []
        });
        $jq( '#calendar11' ).calendario({
            caldata : events,
            month: 11,
            fillEmpty: false,
            year: anio,
            displayWeekAbbr : true,
            events: []
        });
        $jq( '#calendar12' ).calendario({
            caldata : events,
            month: 12,
            fillEmpty: false,
            year: anio,
            displayWeekAbbr : true,
            events: []
        });

        for (var i = 0; i < 13; i++) {
          var custom_month = "#custom-inner"+i+" #custom-month";
          var calendar_x  = '#calendar'+i;
          $jq( custom_month ).html( meses[i]);
        };
        $scope.infoEvent = window.localStorage.getItem("event_CLICK");
        if($scope.infoEvent == 1){
          $jq( "time[datacalndario='0']" ).click();
          //console.log($jq( "time[datacalndario='0']") );
          $scope.infoEvent = window.localStorage.setItem("event_CLICK",0);
        }
      }
    }
    $scope.nextSlider = function(){
      $scope.selectModalSlider.next();
    }
    $scope.prevSlider = function(){
      $scope.selectModalSlider.previous();
    }
    $scope.mesSeleccion = function(mes_seleccionado){
      $scope.mesControl = mes_seleccionado;
      $jq( '#calendar_mes' ).calendario({
          caldata : events,
          fillEmpty: false,
          month: mes_seleccionado,
          year: anio,
          displayWeekAbbr : true,
          events: ['click', 'focus']
      });
      $scope.date = new Date();
      $scope.hora = "";
      $scope.mes = meses[mes_seleccionado];
      $jq('div.fc-row > div').on('onDayClick.calendario', function(e, dateprop) {
        //console.log(JSON.stringify(dateprop));
        //////console.log(dateprop.data.startTime.sort(function(a, b){return a-b}));
        //$jq(this).children("time").append("<p>"+dateprop.data.startTime[i].toISOString()+"</p>")
        $scope.start_Date = new Date();
        $scope.start_Date.setDate(dateprop.day);
        $scope.start_Date.setMonth(parseInt(dateprop.month)-1);
        $scope.end_Date = new Date();
        $scope.end_Date.setDate(dateprop.day);
        $scope.end_Date.setMonth(parseInt(dateprop.month)-1);
        $jq(".eventos_container ul li .eventos_individuals").html("");
        $jq(".eventos_container > h1").html(dateprop.weekdayname+","+dateprop.day+" de "+dateprop.monthname+" de "+dateprop.year);
        if( dateprop.data.content) {
          if( dateprop.data.content.length > 0 ) {
            for (var i = 0; i < dateprop.data.content.length; i++) {
              $scope.date = new Date(dateprop.data.startTime[i]);
              $jq(".eventos_container ul li").each(function(){
                  if($jq(this).attr("datetime") == $scope.date.getHours()){
                      if($scope.date.getHours() > 12){
                        $jq(this).children(".eventos_individuals").append("<time datacalndario='"+dateprop.data.note[i]+"' ><h1>"+formatDate($scope.date)+"</h1><span></span><p>"+dateprop.data.content[i]+"</p></time>");
                      }else if($scope.date.getHours() == 00){
                        $jq(this).children(".eventos_individuals").append("<time datacalndario='"+dateprop.data.note[i]+"' ><h1>"+formatDate($scope.date)+"</h1><span></span><p>"+dateprop.data.content[i]+"</p></time>");
                      } else if($scope.date.getHours() == 12){
                        $jq(this).children(".eventos_individuals").append("<time datacalndario='"+dateprop.data.note[i]+"' ><h1>"+formatDate($scope.date)+"</h1><span></span><p>"+dateprop.data.content[i]+"</p></time>");
                      }else{
                        $jq(this).children(".eventos_individuals").append("<time datacalndario='"+dateprop.data.note[i]+"' ><h1>"+formatDate($scope.date)+"</h1><span></span><p>"+dateprop.data.content[i]+"</p></time>");
                      }
                  }
              })
            }
            $scope.dateIndiv=(dateprop);
          }
        }
        $scope.slideTo(2);
      });

      $scope.slideToCalendar(1);
    }

    $jq(document).on("click",".eventos_container ul li time", function(){
      for (var i = 0; i < $scope.dateIndiv.data.note.length; i++) {
        if($jq(this).attr("datacalndario") == $scope.dateIndiv.data.note[i]){
          ////console.log($scope.dateIndiv.data.content[i])
          $scope.edit_startTime = new Date($scope.dateIndiv.data.startTime[i]);
          $scope.edit_endTime = new Date($scope.dateIndiv.data.endTime[i]);
          $scope.start_Time = formatDate($scope.edit_startTime);
          $scope.end_Time = formatDate($scope.edit_endTime);
          $scope.hours_end = $scope.edit_endTime.getHours();
          $scope.minutes_end = $scope.edit_endTime.getMinutes();
          $scope.hours_start = $scope.edit_startTime.getHours();
          $scope.minutes_start = $scope.edit_startTime.getMinutes();
          dia = new Date($scope.edit_startTime).getDay();
          $scope.start_Date = dia_semana[dia]+" "+$scope.edit_startTime.getDate()+"/"+( parseInt($scope.edit_startTime.getMonth()) + 1)+"/"+$scope.edit_startTime.getFullYear();
          $scope.TMPdiaIni = $scope.edit_startTime.getDate();
          $scope.TMPmesIni = meses[parseInt($scope.edit_startTime.getMonth())+1];
          dia = new Date($scope.edit_endTime).getDay();
          $scope.end_Date = dia_semana[dia]+" "+$scope.edit_endTime.getDate()+"/"+( parseInt($scope.edit_endTime.getMonth()) + 1)+"/"+$scope.edit_endTime.getFullYear();
          $scope.TMPdiaEnd = $scope.edit_endTime.getDate();
          $scope.TMPmesEnd = meses[parseInt($scope.edit_endTime.getMonth())+1];
          $scope.repeticion.myRepeticion = $scope.repeticion[$scope.dateIndiv.data.recordatorio[i]]; // red
          $scope.model.titulo = $scope.dateIndiv.data.content[i];
          $scope.ID = $scope.dateIndiv.data.note[i];
          
        }

      };

      $scope.slideTo(4);
    })

    $scope.EliminarEvento = function(){
      //console.log($scope.ID);
      db.transaction(DeleteEvent, errorCB, successDeleteEvent);
    }
    $scope.UpdateEvent = function(){
      //console.log($scope.ID);
      db.transaction(UpdateEvent, errorCB, successUpdateEvent);
    }
    /*$scope.$on('modal.hidden', function() {
      $jq( '.flecha_down' ).show();
    });
    $ionicModal.fromTemplateUrl('templates/calendario_meses.html', {
      scope: $scope,
      backdropClickToClose: false,
      animation: 'slide-in-right'
    }).then(function(modal) {
      $scope.modal = modal;
      $scope.selectModalSlider = $ionicSlideBoxDelegate.$getByHandle('modalSlider');
      $scope.selectModalSlider.enableSlide(false);
    });*/
    $scope.openModal = function(){
      //$scope.modal.show();
      $scope.slideTo(0);
    }

    $scope.closeModal = function(){
      //$scope.modal.hide();
    }
    $scope.myGadget = function(){
    }

    function onSuccess(date) {
        //alert('Selected date: ' + date);
    }

    function onError(error) { // Android only
        //alert('Error: ' + error);
    }

    $scope.datepickerObject = {
      titleLabel: "Fecha Evento",  //Optional
      todayLabel: false,  //Optional
      closeLabel: 'Cancelar',  //Optional
      setLabel: 'Aceptar',  //Optional
      setButtonType : 'button-positive',  //Optional
      todayButtonType : 'button-positive',  //Optional
      closeButtonType : 'button-positive',  //Optional
      inputDate: new Date(),  //Optional
      mondayFirst: true,  //Optional
      templateType: 'popup', //Optional
      showTodayButton: 'true', //Optional
      modalHeaderColor: 'bar-positive', //Optional
      modalFooterColor: 'bar-positive', //Optional
      from: new Date(anio, 0, 1), //Optional
      to: new Date(anio, 11, 31),  //Optional
      callback: function (date) {  //Mandatory
        if (date) {
          $scope.start_DateCreate = new Date(date);
          $scope.edit_startTime = new Date(date);
          dia = new Date(date).getDay();
          $scope.start_Date = dia_semana[dia]+" "+$scope.start_DateCreate.getDate()+"/"+(parseInt($scope.start_DateCreate.getMonth()) + 1)+"/"+$scope.start_DateCreate.getFullYear();
        };

      },
      dateFormat: 'dd-MM-yyyy', //Optional
      closeOnSelect: false, //Optional
    };

    $scope.datepickerObject2 = {
      titleLabel: "Fecha Evento",  //Optional
      todayLabel: false,  //Optional
      closeLabel: 'Cancelar',  //Optional
      setLabel: 'Aceptar',  //Optional
      setButtonType : 'button-positive',  //Optional
      todayButtonType : 'button-positive',  //Optional
      closeButtonType : 'button-positive',  //Optional
      inputDate: new Date(),  //Optional
      mondayFirst: true,  //Optional
      templateType: 'popup', //Optional
      showTodayButton: 'true', //Optional
      modalHeaderColor: 'bar-positive', //Optional
      modalFooterColor: 'bar-positive', //Optional
      from: new Date(anio, 0, 1), //Optional
      to: new Date(anio, 11, 31),  //Optional
      callback: function (date) {  //Mandatory
        if (date) {
          $scope.end_DateCreate = new Date(date);
          $scope.edit_endTime = new Date(date);
          dia = new Date(date).getDay();
          $scope.end_Date = dia_semana[dia]+" "+$scope.end_DateCreate.getDate()+"/"+(parseInt($scope.end_DateCreate.getMonth()) + 1)+"/"+$scope.end_DateCreate.getFullYear();
        };

      },
      dateFormat: 'dd-MM-yyyy', //Optional
      closeOnSelect: false, //Optional
    };

    $scope.timePickerObject = {
      inputEpochTime: ((new Date()).getHours() * 60 * 60),  //Optional
      step: 1,  //Optional
      format: 12,  //Optional
      titleLabel: "Hora Evento",  //Optional
      setLabel: 'Aceptar',  //Optional
      closeLabel: 'Cancelar',  //Optional
      modalHeaderColor: 'bar-positive', //Optional
      modalFooterColor: 'bar-positive', //Optional
      callback: function (time) {    //Mandatory
        if (time) {
          //console.log(time);
          $scope.start_TimeCreate = new Date(time* 1000);
          $scope.hours_start = $scope.start_TimeCreate.getUTCHours();
          $scope.minutes_start = $scope.start_TimeCreate.getUTCMinutes();
          $scope.start_Time = formatDateUTC($scope.start_TimeCreate);
          //console.log($scope.hours_start);
        };

      }
    };

    $scope.timePickerObject2 = {
      inputEpochTime: ((new Date()).getHours() * 60 * 60),  //Optional
      step: 1,  //Optional
      format: 12,  //Optional
      titleLabel: "Hora Evento",  //Optional
      setLabel: 'Aceptar',  //Optional
      closeLabel: 'Cancelar',  //Optional
      modalHeaderColor: 'bar-positive', //Optional
      modalFooterColor: 'bar-positive', //Optional
      callback: function (time) {    //Mandatory
        if (time) {
          $scope.end_TimeCreate = new Date(time * 1000);
          $scope.hours_end = $scope.end_TimeCreate.getUTCHours();
          $scope.minutes_end = $scope.end_TimeCreate.getUTCMinutes();
          $scope.end_Time = formatDateUTC($scope.end_TimeCreate);
          //console.log($scope.hours_end);
        }
      }
    };
    
    $scope.crearEvento = function(){
      db.transaction(InsertEvent, errorInsertEvent, successInsertEvent);
    }

    function InsertEvent(tx) {
      tx.executeSql('INSERT INTO DEMO (ID, dia_inicio, mes_inicio, ano_inicio,hora_inicio,minutos_inicio,dia_fin,mes_fin,ano_fin,hora_fin,minutos_fin,titulo,descripcion,repeticion,recordatorio,chica) VALUES (null, '+$scope.start_DateCreate.getDate()+','+$scope.start_DateCreate.getMonth()+','+$scope.start_DateCreate.getFullYear()+','+$scope.hours_start+','+$scope.minutes_start+','+$scope.end_DateCreate.getDate()+','+$scope.end_DateCreate.getMonth()+','+$scope.end_DateCreate.getFullYear()+','+$scope.hours_end+','+$scope.minutes_end+',"'+($scope.model.titulo == "" ? $scope.model.titulo="Sin Titulo" : $scope.model.titulo)+'","'+($scope.model.titulo == "" ? $scope.model.titulo="Sin Titulo" : $scope.model.titulo)+'",'+$scope.repeticion.myRepeticion.value+',10,"'+$scope.chicaVozEvento.chicaVozEventoModel.value+'")');
      sonido = "file://eventos/"+$scope.chicaVozEvento.chicaVozEventoModel.value;
      var now = new Date().getTime(),
      hora = new Date(now);
      hora.setHours($scope.hours_start,$scope.minutes_start,00);
      $scope.ID = window.localStorage.getItem("last_evento_ID");
      var options = {
      id:$scope.ID,  // A unique id of the notification, best to use a numeric value
      at: hora,
      text:($scope.model.titulo == "" ? $scope.model.titulo="Sin Titulo" : $scope.model.titulo),  // The message
      title:($scope.model.titulo == "" ? $scope.model.titulo="Sin Titulo" : $scope.model.titulo),  // The title of the message
      every:$scope.repeticion.myRepeticion.repeat,  // 'minute', 'hour', 'day', 'week', 'month', 'year'
      icon:"res://drawable-land-hdpi/icon.png",
      sound:sonido,  // The sound to be played (null means no sound)
      autoClear:true, // The notification is canceled when the user clicks it
      ongoing:false, // Prevent clearing the notification (Android only)
      };
      cordova.plugins.notification.local.schedule(options);
    }
    function DeleteEvent(tx) {
        tx.executeSql('DELETE FROM DEMO WHERE ID='+$scope.ID);
        cordova.plugins.notification.local.cancel($scope.ID,
          function() {}
        );
    }
    function UpdateEvent(tx) {
      tx.executeSql('UPDATE DEMO SET dia_inicio='+$scope.edit_startTime.getDate()+', mes_inicio='+$scope.edit_startTime.getMonth()+', ano_inicio='+$scope.edit_startTime.getFullYear()+',hora_inicio='+$scope.hours_start+',minutos_inicio='+$scope.minutes_start+',dia_fin='+$scope.edit_endTime.getDate()+',mes_fin='+$scope.edit_endTime.getMonth()+',ano_fin='+$scope.edit_endTime.getFullYear()+',hora_fin='+$scope.hours_end+',minutos_fin='+$scope.minutes_end+',titulo="'+($scope.model.titulo == "" ? $scope.model.titulo="Sin Titulo" : $scope.model.titulo)+'",descripcion="'+($scope.model.titulo == "" ? $scope.model.titulo="Sin Titulo" : $scope.model.titulo)+'",repeticion='+$scope.repeticion.myRepeticion.value+',recordatorio=10,chica="'+$scope.chicaVozEvento.chicaVozEventoModel.value+'" WHERE ID='+$scope.ID);
      sonido = "file://eventos/"+$scope.chicaVozEvento.chicaVozEventoModel.value;
      var now = new Date().getTime(),
      hora = new Date(now);
      hora.setHours($scope.hours_start,$scope.minutes_start,00);
      var options = {
        id:$scope.ID,  // A unique id of the notification, best to use a numeric value
        at: hora,
        text:($scope.model.titulo == "" ? $scope.model.titulo="Sin Titulo" : $scope.model.titulo),  // The message
        title:($scope.model.titulo == "" ? $scope.model.titulo="Sin Titulo" : $scope.model.titulo),  // The title of the message
        every:$scope.repeticion.myRepeticion.repeat,  // 'minute', 'hour', 'day', 'week', 'month', 'year'
        icon: "res://drawable-land-hdpi/icon.png",
        sound:sonido,  // The sound to be played (null means no sound)
        autoClear:true, // The notification is canceled when the user clicks it
        ongoing:true, // Prevent clearing the notification (Android only)
      };
      cordova.plugins.notification.local.update(options);
    }
    function errorInsertEvent(err) {
        //alert("Error processing SQL: "+err.code);
    }
    function successInsertEvent() {
        db.transaction(queryDates, errorCB);
        navigator.notification.alert('Evento creado', null, 'Eventos', 'OK');
        $scope.start_Date = new Date();
        $scope.end_Date = new Date();
    }
    function successDeleteEvent() {
        db.transaction(queryDates, errorCB);
        navigator.notification.alert('Evento eliminado', null, 'Eventos', 'OK');
        $scope.slideTo(1);
    }
    function successUpdateEvent() {
        db.transaction(queryDates, errorCB);
        navigator.notification.alert('Evento actualizado', null, 'Eventos', 'OK');
        $scope.slideTo(1);
    }
    function errorCB(err) {
        //alert("Error processing SQL: "+err.code);
    }
    function queryDates(tx){
      tx.executeSql ('SELECT * FROM DEMO', [], successDate, errorCB);
    }

    function successDate(tx, results) {
      events = {};
        if(results.rows.length > 0){
            for (var i = 0; i < results.rows.length; i++) {
            //console.log ("ID=" + results.rows.item(i).ID+" Fecha Inicio= "+results.rows.item(i).mes_inicio+"-"+results.rows.item(i).dia_inicio+"-"+results.rows.item(i).ano_inicio);
                var hora_inicio =results.rows.item(i).hora_inicio;
                var minutos_inicio = results.rows.item(i).minutos_inicio;
                var hora_fin = results.rows.item(i).hora_fin;
                var minutos_fin = results.rows.item(i).minutos_fin;
                var mes_inicio = results.rows.item(i).mes_inicio;
                var dia_inicio = results.rows.item(i).dia_inicio;
                var ano_inicio = results.rows.item(i).ano_inicio;
                var ano_fin = results.rows.item(i).ano_fin;
                var dia_fin = results.rows.item(i).dia_fin;
                var mes_fin = results.rows.item(i).mes_fin;
                var startDate = (mes_inicio < 10 ? '0' + (parseInt(mes_inicio)+1 ) : (parseInt(mes_inicio)+1)) + "-" + ((dia_inicio) < 10 ? '0' + (dia_inicio) : (dia_inicio)) + "-" + ano_inicio;
                var endDate = mes_fin+"-"+dia_fin+"-"+ano_fin;
                var dtstart_calendario = hora_inicio+":"+minutos_inicio;
                var dtend_calendario = hora_fin+":"+minutos_fin;
                var title = results.rows.item(i).titulo;
                var ID=results.rows.item(i).ID;
                var last_evento_ID = parseInt(ID) + 1;
                window.localStorage.setItem("last_evento_ID", last_evento_ID);
                var repeticion = results.rows.item(i).repeticion;
                //console.log ("ID=" + results.rows.item(i).ID+" Fecha Inicio= "+startDate);
                if((startDate in events)){
                    events[startDate].push({content: title, startTime: dtstart_calendario, endTime: dtend_calendario, note: ID, recordatorio:repeticion});
                }else{
                    events[startDate]=[{content: title, startTime: dtstart_calendario, endTime: dtend_calendario, note: ID, recordatorio:repeticion}];
                }
            }
        }else{
            window.localStorage.setItem("last_evento_ID", 555);
        }
      $scope.slideTo(1);
    }

})

.controller('despertadorHomeTabCtrl', function($scope, $state,$ionicSlideBoxDelegate,$rootScope) {
  $scope.$on('$ionicView.enter', function(){
    analytics.sendAppView("/Despertador", function(){},function(){});
  })
  
  $scope.toggle = null;
  $scope.insertar = 0;
  $scope.chicaSeleccionAlarma =[
    {name: "Karen"},
    {name: "Grabiela"}
  ];
  $scope.repeticionAlarma =[
    {name: "Diaria"}
  ];
  $scope.$on('$ionicView.enter', function(){
    $scope.tipoCalendarioSeleccion = window.localStorage.getItem("Calendario");
    $scope.chicasCalendarPremium = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
    $scope.chicasCalendarPremiumDate = new Date().getMonth();
    if($scope.tipoCalendarioSeleccion == 1){
      $scope.layout = 'CalendarioPremium/'+$scope.chicasCalendarPremium[$scope.chicasCalendarPremiumDate];
    }else if($scope.tipoCalendarioSeleccion == 2 || $scope.tipoCalendarioSeleccion == 3){
      $scope.chicaSeleccion =[
        {name: "Karen"},
        {name: "Georgina"},
        {name: "Grabiela"},
        {name: "Gabriella"},
        {name: "Anmarie"},
        {name: "Grupal"}

      ];
      $scope.chicaSeleccionada = window.localStorage.getItem("chicaSeleccionada");
      $scope.fotoSeleccion = window.localStorage.getItem("fotoSeleccion");
      $scope.chicaSeleccion.Mychica =  $scope.chicaSeleccion[$scope.chicaSeleccionada];
      $scope.layout = 'DetalleChicaEscogida/'+$scope.chicaSeleccion.Mychica.name+'/EscogeChica_'+$scope.chicaSeleccion.Mychica.name+'_'+(parseInt($scope.fotoSeleccion)+1);
    }
    db.transaction(queryAlarmas, errorQueryAlarmas, successQueryAlarmas);
  })
  $scope.repeticionAlarma.Alarma = $scope.repeticionAlarma[0];
  $scope.chicaSeleccionAlarma.VozAlarma = $scope.chicaSeleccionAlarma[0];
  $scope.mes = "Despertador";
  $scope.selectModalSliderDespertador = $ionicSlideBoxDelegate.$getByHandle('modalSliderDespertador');
  $scope.nombreAlarma = {};
  $scope.nombreAlarma.model = "";
  $scope.inputHoraAlarma = new Date().getHours();
  $scope.inputMinutosAlarma = new Date().getMinutes();
    if($scope.inputHoraAlarma.toString().length < 2){
        $scope.inputHoraAlarma="0"+$scope.inputHoraAlarma;
    }
    if($scope.inputMinutosAlarma.toString().length < 2){
        $scope.inputMinutosAlarma="0"+$scope.inputMinutosAlarma;
    }
  $scope.dateGeneral = new Date();
  $scope.HoraAlarma = formatHour($scope.dateGeneral);
  $scope.MinutosAlarma = $scope.dateGeneral.getMinutes();
  $scope.amPm = formatDate($scope.dateGeneral);
  $scope.amPm = $scope.amPm.split(" ");
  $scope.amPm = $scope.amPm[1];
  $scope.lockSlide = function(){
    $scope.selectModalSliderDespertador.enableSlide(false);
  }
  
  $scope.slideTo = function(index){
    $scope.selectModalSliderDespertador.slide(index);
  }
  $scope.timePickerDespertador = {
    inputEpochTime: ((new Date()).getHours() * 60 * 60),  //Optional
    step: 1,  //Optional
    format: 12,  //Optional
    titleLabel: 'HORA ALARMA',  //Optional
    setLabel: 'Aceptar',  //Optional
    closeLabel: 'Cancelar',  //Optional
    setButtonType : 'button-positive',  //Optional
    todayButtonType : 'button-positive',  //Optional
    closeButtonType : 'button-positive',  //Optional
    callback: function (time) {    //Mandatory
      if (time) {
        //console.log(time);
        $scope.alarmTime = new Date(time* 1000);
        $scope.alarmTimeFormat = formatDateUTC($scope.alarmTime);
        $scope.HoraAlarma = $scope.alarmTime.getUTCHours();
        $scope.MinutosAlarma = $scope.alarmTime.getUTCMinutes();
        $scope.inputHoraAlarma = $scope.alarmTimeFormat.split(":");
        $scope.inputHoraAlarma = $scope.inputHoraAlarma[0];
        $scope.inputMinutosAlarma = $scope.alarmTimeFormat.split(":");
        $scope.inputMinutosAlarma = $scope.inputMinutosAlarma[1].split(" ");
        $scope.inputMinutosAlarma = $scope.inputMinutosAlarma[0];
        $scope.amPm = $scope.alarmTimeFormat.split(" ");
        $scope.amPm = $scope.amPm[1];
        //console.log($scope.hours_start);
      };

    }
  };
  $scope.crearAlarma = function(){

    if($scope.alarmTime){
    }else{
      $scope.HoraAlarma = new Date().getHours();
      $scope.MinutosAlarma = new Date().getMinutes();
    }
    //console.log( $scope.chicaSeleccionAlarma.VozAlarma.name);

    $scope.insertar = 1;
    db.transaction(queryAlarmas, errorQueryAlarmas, successQueryAlarmas);
  }
  
  function errorQueryAlarmas(err) {
      //alert("Error processing SQL: "+err.code);
  }
  function queryAlarmas(tx){
    tx.executeSql ('SELECT * FROM ALARMAS', [], successQueryAlarmas, errorQueryAlarmas);
  }
  $scope.toggle = [];

  function successQueryAlarmas(tx, results) {
    var alarmas_array = [10002,10003,10004,10005,10006];
    if(results.rows.length>0){
      
        $scope.toggle = null;
        $scope.toggle = [];

        for (var i = 0; i < results.rows.length; i++) {
          var ID=results.rows.item(i).IDA;
          var checked = results.rows.item(i).TOGGLE;
          var titulo = results.rows.item(i).TITULO;
          var mensaje = "alarma";
          var hora = results.rows.item(i).HORA;
          var minutos = results.rows.item(i).MINUTOS;
          var formato = results.rows.item(i).FORMATO;
          var chica = results.rows.item(i).CHICA;
          if(checked == "true"){
            checked= true;
          }else{
            checked = false;
          }
          $scope.$apply(function() {
              $scope.toggle.push({ID:ID, checked:checked,TITULO:titulo, MENSAJE:mensaje, HORA:hora, MINUTOS:minutos, FORMATO: formato, CHICA: chica});
          });
          
          if(i == (results.rows.length-1) ){
            if(results.rows.length  < 5){
                i++;
                $scope.ID_ALARMA = alarmas_array[i];
                
                if($scope.insertar == 1){
                  if($scope.nombreAlarma.model ==""){
                    $scope.nombreAlarma.model = "Alarma Sin Título";
                  }
                  if($scope.HoraAlarma > 11 || $scope.HoraAlarma == 0 ){
                    $scope.amPm= "pm";
                  }else{
                    $scope.amPm = "am";
                  }
                  db.transaction(InsertAlarma, errorInsertAlarma, successInsertAlarma);
                }
            }else{
                navigator.notification.alert('Máximo de alarmas', null, 'Alarmas', 'OK');
            }
          }
       }
    }else{
        if($scope.HoraAlarma > 11 || $scope.HoraAlarma == 0 ){
            $scope.amPm= "pm";
        }else{
            $scope.amPm = "am";
        }
        if($scope.insertar == 1){
            if($scope.nombreAlarma.model ==""){
                $scope.nombreAlarma.model = "Alarma Sin Título";
            }
            $scope.ID_ALARMA = 10002;
            $scope.slideTo(0);
            db.transaction(InsertAlarma, errorInsertAlarma, successInsertAlarma);
        }else{
            $scope.slideTo(1);
        }
    }
  }
  function InsertAlarma(tx) {
    if($scope.HoraAlarma.toString().length < 2){
        $scope.HoraAlarma="0"+$scope.HoraAlarma;
    }
    if($scope.MinutosAlarma.toString().length < 2){
        $scope.MinutosAlarma="0"+$scope.MinutosAlarma;
    }
    tx.executeSql('INSERT INTO ALARMAS(ID, IDA, TITULO, CHICA, REPETICION, HORA, MINUTOS, FORMATO, TOGGLE) VALUES (NULL,'+$scope.ID_ALARMA+',"'+$scope.nombreAlarma.model+'","'+$scope.chicaSeleccionAlarma.VozAlarma.name+'","","'+$scope.HoraAlarma+'","'+$scope.MinutosAlarma+'","'+$scope.amPm+'","true")');
  }
  function errorInsertAlarma(err){
    navigator.notification.alert(err, null, 'Alarmas', 'OK');
  }
  function successInsertAlarma(){
    if($scope.HoraAlarma > 11 || $scope.HoraAlarma == 0 ){
      $scope.amPm= "pm";
    }else{
      $scope.amPm = "am";
    }
    window.wakeuptimer.wakeup( successDespertador,  
    errorDespertador, 
       // a list of alarms to set
       {
            alarms : [{
                type : 'onetime',
                time : { hour : $scope.HoraAlarma, minute : $scope.MinutosAlarma },
                extra : { message : 'alarma', chica : $scope.chicaSeleccionAlarma.VozAlarma.name, titulo : $scope.nombreAlarma.model, hora : $scope.HoraAlarma, minutos : $scope.MinutosAlarma, formato:$scope.amPm, ID: $scope.ID_ALARMA}, 
                message : 'Alarm has expired!'
           }] 
       }
    )
    $scope.nombreAlarma.model = "";
    $scope.insertar = 0;
    $scope.slideTo(0);
    db.transaction(queryAlarmas, errorQueryAlarmas, successQueryAlarmas);
    //alert("succes alarma");

  }
  $scope.cancelarAlarma = function(){
   window.wakeuptimer.cancelar( successCancelar, errorCancelar,{});

  }
  $scope.slideChangedDespertador = function(index) {
    $scope.slideIndex = index;
    //console.log(index);
    if(index == 1 || index==0){
      $scope.mes = "Despertador";
      $jq(".despertador_home .barra_volverGeneral").hide();
      $jq(".despertador_home .controles").show();
    }else if(index == 2){
      $scope.barraGeneral = "Crear alarma";
      $jq(".despertador_home .barra_volverGeneral").show();
      $jq(".despertador_home .controles").hide();
    }else if(index == 3){
      $scope.barraGeneral = "Actualizar alarma";
      $jq(".despertador_home .barra_volverGeneral").show();
      $jq(".despertador_home .controles").hide();
    }
  }
  function successCancelar(evento){
    navigator.notification.alert('Alarma cancelada', null, 'Alarmas', 'OK');
    window.wakeuptimer.cancelar( function(){}, function(){},{});
    //console.log(evento);
  }
  function errorCancelar(evento){
    //console.log(evento);
  }
  var chica_update="";
  var toggle_update = "";
  var titulo_update = "";
  var hora_update = "";
  var minutos_update = "";
  var formato_update = "";
  var ida_update = "";
  $scope.estadoAlarma = function(ID){
    for (var i = 0; i < $scope.toggle.length; i++) {
      if($scope.toggle[i].ID == ID){
        chica_update = $scope.toggle[i].CHICA;
        toggle_update = $scope.toggle[i].checked;
        titulo_update = $scope.toggle[i].TITULO;
        hora_update = $scope.toggle[i].HORA;
        minutos_update = $scope.toggle[i].MINUTOS;
        formato_update = $scope.toggle[i].FORMATO;
        ida_update = $scope.toggle[i].ID;
        db.transaction(updateAlarma, errorUpdateAlarma, successUpdateAlarma);
      }
    } 
  }
  $scope.update_alarma = function(ID){
    for (var i = 0; i < $scope.toggle.length; i++) {
      if($scope.toggle[i].ID == ID){
        chica_update = $scope.toggle[i].CHICA;
        toggle_update = $scope.toggle[i].checked;
        titulo_update = $scope.toggle[i].TITULO;
        hora_update = $scope.toggle[i].HORA;
        minutos_update = $scope.toggle[i].MINUTOS;
        formato_update = $scope.toggle[i].FORMATO;
        ida_update = $scope.toggle[i].ID;
        $scope.HoraAlarma = hora_update;
        $scope.MinutosAlarma = minutos_update;
        $scope.nombreAlarma.model = titulo_update;
        $scope.inputHoraAlarma = hora_update;
        $scope.inputMinutosAlarma = minutos_update;
        $scope.slideTo(3);
      }
    } 
  }
  $scope.updateNow = function(){
    chica_update = $scope.chicaSeleccionAlarma.VozAlarma.name;
    titulo_update = $scope.nombreAlarma.model;
    hora_update = $scope.HoraAlarma;
    minutos_update = $scope.MinutosAlarma;
    if(hora_update > 11 || hora_update == 0 ){
      $scope.amPm= "pm";
    }else{
      $scope.amPm = "am";
    }
    formato_update = $scope.amPm;
    db.transaction(updateAlarma, errorUpdateAlarma, successUpdateAlarma);
    navigator.notification.alert('Actualizada', null, 'Alarmas', 'OK');
  }
  function updateAlarma(tx){
    if(minutos_update.toString().length < 2){
      minutos_update="0"+minutos_update;
    }
    if(hora_update.toString().length < 2){
      hora_update="0"+hora_update;
    }
    tx.executeSql('UPDATE ALARMAS SET IDA='+ida_update+',TITULO="'+titulo_update+'", CHICA="'+chica_update+'", HORA="'+hora_update+'", MINUTOS="'+minutos_update+'", FORMATO="'+formato_update+'", TOGGLE="'+toggle_update+'" WHERE IDA='+ida_update);
  }
  function errorUpdateAlarma(err){
    navigator.notification.alert(err, null, 'Alarmas', 'OK');
  }
  function successUpdateAlarma(){
    if(hora_update > 11 || hora_update == 0 ){
      $scope.amPm= "pm";
    }else{
      $scope.amPm = "am";
    }
    if(toggle_update == false){
      window.wakeuptimer.wakeup( function(){},  
      errorDespertador, 
         // a list of alarms to set
         {
          alarms : [{
              type : 'cancelar',
              time : { hour : hora_update, minute : minutos_update },
              extra : { message : 'alarma', chica : chica_update, titulo : titulo_update, hora : hora_update, minutos : minutos_update, formato:formato_update, ID: ida_update}, 
              message : 'Alarm has expired!'
          }] 
         }
      )
    }else{
      window.wakeuptimer.wakeup( successDespertador2,  
      errorDespertador, 
         // a list of alarms to set
         {
              alarms : [{
                  type : 'onetime',
                  time : { hour : hora_update, minute : minutos_update },
                  extra : { message : 'alarma', chica : chica_update, titulo : titulo_update, hora : hora_update, minutos : minutos_update, formato:formato_update, ID: ida_update}, 
                  message : 'Alarm has expired!'
             }] 
         }
      )  
    }
    db.transaction(queryAlarmas, errorQueryAlarmas, successQueryAlarmas);
  }
  
})

.controller('bodegaHomeTabCtrl', function($scope, $state) {

})
.controller('ControllerKaren', function($scope, $state ,$ionicSlideBoxDelegate) {
  $scope.$on('$ionicView.enter', function(){
    analytics.sendAppView("/Bodega", function(){},function(){});


    $scope.selectModalSlider = $ionicSlideBoxDelegate.$getByHandle('fotosSlider');
    $scope.selectModalSlider.enableSlide(false);
  })
  
  //alert($jq(document).width()+":"+$jq(document).height());
  $scope.style1="";
  $scope.layout = 'FONDO-PAPEL';
  $scope.filtro = 1;
  $scope.sonidos = [];
  $scope.imgSmall = ["FotosBodega_SMAILL_Karen_1","FotosBodega_SMAILL_Karen_2","FotosBodega_SMAILL_Karen_3","FotosBodega_SMAILL_Karen_4","FotosBodega_SMAILL_Karen_5","FotosBodega_SMAILL_Karen_6","FotosBodega_SMAILL_Karen_7","FotosBodega_SMAILL_Karen_8","FotosBodega_SMAILL_Karen_9","FotosBodega_SMAILL_Karen_10"];
  $scope.wilfri = [];
  $scope.chicaSeleccion =[
    {name: "Gabriella"},
    {name: "Grabiela"},
    {name: "Georgina"},
    {name: "Anmarie"}
    // {name: "Karen"}

  ];
  $scope.titulo_filtro ="Fotos";
  $scope.chicaSeleccion.Mychica =  $scope.chicaSeleccion[0];
  $scope.tamanoSeleccion =[
    {name: "ImagesMedium", cambio:"MEDIUM"},
    {name: "ImagesLarge", cambio:"LARGE"}

  ];
  $scope.tamanoSeleccion.Tamano =  $scope.tamanoSeleccion[0];
  $scope.hiper = [
    {link: 'http://www.polarpilsen.com/'+$scope.tamanoSeleccion.Tamano.name+'/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_'+$scope.tamanoSeleccion.Tamano.cambio+'_'+$scope.chicaSeleccion.Mychica.name+'_1.jpg'},
    {link: 'http://www.polarpilsen.com/'+$scope.tamanoSeleccion.Tamano.name+'/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_'+$scope.tamanoSeleccion.Tamano.cambio+'_'+$scope.chicaSeleccion.Mychica.name+'_2.jpg'},
    {link: 'http://www.polarpilsen.com/'+$scope.tamanoSeleccion.Tamano.name+'/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_'+$scope.tamanoSeleccion.Tamano.cambio+'_'+$scope.chicaSeleccion.Mychica.name+'_3.jpg'},
    {link: 'http://www.polarpilsen.com/'+$scope.tamanoSeleccion.Tamano.name+'/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_'+$scope.tamanoSeleccion.Tamano.cambio+'_'+$scope.chicaSeleccion.Mychica.name+'_4.jpg'},
    {link: 'http://www.polarpilsen.com/'+$scope.tamanoSeleccion.Tamano.name+'/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_'+$scope.tamanoSeleccion.Tamano.cambio+'_'+$scope.chicaSeleccion.Mychica.name+'_5.jpg'},
    {link: 'http://www.polarpilsen.com/'+$scope.tamanoSeleccion.Tamano.name+'/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_'+$scope.tamanoSeleccion.Tamano.cambio+'_'+$scope.chicaSeleccion.Mychica.name+'_6.jpg'},
    {link: 'http://www.polarpilsen.com/'+$scope.tamanoSeleccion.Tamano.name+'/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_'+$scope.tamanoSeleccion.Tamano.cambio+'_'+$scope.chicaSeleccion.Mychica.name+'_7.jpg'},
    {link: 'http://www.polarpilsen.com/'+$scope.tamanoSeleccion.Tamano.name+'/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_'+$scope.tamanoSeleccion.Tamano.cambio+'_'+$scope.chicaSeleccion.Mychica.name+'_8.jpg'},
    {link: 'http://www.polarpilsen.com/'+$scope.tamanoSeleccion.Tamano.name+'/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_'+$scope.tamanoSeleccion.Tamano.cambio+'_'+$scope.chicaSeleccion.Mychica.name+'_9.jpg'},
    {link: 'http://www.polarpilsen.com/'+$scope.tamanoSeleccion.Tamano.name+'/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_'+$scope.tamanoSeleccion.Tamano.cambio+'_'+$scope.chicaSeleccion.Mychica.name+'_10.jpg'}
  ];
  $scope.alerta = function(){
    $scope.wilfri.Gabriella = [
        {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_1.jpg'},
        {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_2.jpg'},
        {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_3.jpg'},
        {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_4.jpg'},
        {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_5.jpg'},
        {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_6.jpg'},
        {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_7.jpg'}
    ];
    $scope.wilfri.Georgina = [
        {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_1.jpg'},
        {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_2.jpg'},
        {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_3.jpg'},
        {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_4.jpg'}
    ];
    $scope.wilfri.Anmarie = [
        {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_1.jpg'},
        {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_2.jpg'},
        {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_3.jpg'},
        {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_4.jpg'},
        {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_5.jpg'},
        {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_6.jpg'},
        {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_7.jpg'},
        {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_8.jpg'}
    ];
    $scope.wilfri.Karen = [
        {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_1.jpg'},
        {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_2.jpg'},
        {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_3.jpg'},
        {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_4.jpg'},
        {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_5.jpg'},
        {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_6.jpg'},
        {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_7.jpg'}
    ];
    $scope.wilfri.Grabiela = [
        {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_1.jpg'},
        {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_2.jpg'},
        {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_3.jpg'},
        {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_4.jpg'},
        {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_5.jpg'},
        {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_6.jpg'}
    ];
    /***********Sonidos*****************/
    $scope.sonidos.Gabriella = [
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'1.mp3', descripcion: 'ANIVERSARIO'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'2.mp3', descripcion: 'CITA MÉDICA'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'3.mp3', descripcion: 'CUMPLEAÑOS'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'4.mp3', descripcion: 'DEPORTE'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'5.mp3', descripcion: 'EVENTO'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'6.mp3', descripcion: 'EXÁMEN'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'7.mp3', descripcion: 'JUEGOS'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'8.mp3', descripcion: 'MEDICINA'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'9.mp3', descripcion: 'REUNIÓN AMIGOS'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'10.mp3', descripcion: 'REUNIÓN'}
    ];
    $scope.sonidos.Georgina = [
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'1.mp3', descripcion: 'CITA MÉDICA'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'2.mp3', descripcion: 'DEPORTE'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'3.mp3', descripcion: 'JUEGOS'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'4.mp3', descripcion: 'MEDICINA'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'5.mp3', descripcion: 'REUNIÓN AMIGOS'}
    ];
    $scope.sonidos.Anmarie = [
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'1.mp3'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'2.mp3'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'3.mp3'}
    ];
    $scope.sonidos.Karen = [
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'1.mp3', descripcion: 'ANIVERSARIO'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'2.mp3', descripcion: 'CITA MÉDICA'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'3.mp3', descripcion: 'CUMPLEAÑOS'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'4.mp3', descripcion: 'DEPORTES'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'5.mp3', descripcion: 'DESPERTADOR'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'6.mp3', descripcion: 'EVENTO'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'7.mp3', descripcion: 'EXÁMEN'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'8.mp3', descripcion: 'JUEGOS'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'9.mp3', descripcion: 'MEDICINA'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'10.mp3', descripcion: 'REUNIÓN AMIGOS'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'11.mp3', descripcion: 'REUNIÓN'}
    ];
    $scope.sonidos.Grabiela = [
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'1.mp3', descripcion: 'ANIVERSARIO'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'2.mp3', descripcion: 'CUMPLEAÑOS'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'3.mp3', descripcion: 'DEPORTES'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'4.mp3', descripcion: 'DESPERTADOR'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'5.mp3', descripcion: 'EVENTO'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'6.mp3', descripcion: 'EXÁMEN'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'7.mp3', descripcion: 'JUEGOS'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'8.mp3', descripcion: 'MEDICINA'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'9.mp3', descripcion: 'REUNIÓN AMIGOS'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'10.mp3', descripcion: 'REUNIÓN'}
    ];
    /*************Fin Sonidos****************/
  }
  $scope.wilfri.Gabriella = [
      {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_1.jpg'},
      {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_2.jpg'},
      {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_3.jpg'},
      {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_4.jpg'},
      {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_5.jpg'},
      {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_6.jpg'},
      {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_7.jpg'}
  ];
  $scope.wilfri.Georgina = [
      {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_1.jpg'},
      {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_2.jpg'},
      {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_3.jpg'},
      {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_4.jpg'}
  ];
  $scope.wilfri.Anmarie = [
      {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_1.jpg'},
      {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_2.jpg'},
      {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_3.jpg'},
      {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_4.jpg'},
      {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_5.jpg'},
      {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_6.jpg'},
      {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_7.jpg'},
      {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_8.jpg'}
  ];
  $scope.wilfri.Karen = [
      {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_1.jpg'},
      {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_2.jpg'},
      {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_3.jpg'},
      {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_4.jpg'},
      {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_5.jpg'},
      {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_6.jpg'},
      {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_7.jpg'}
  ];
  $scope.wilfri.Grabiela = [
      {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_1.jpg'},
      {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_2.jpg'},
      {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_3.jpg'},
      {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_4.jpg'},
      {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_5.jpg'},
      {link: 'ImagesSmall/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_SMAILL_'+$scope.chicaSeleccion.Mychica.name+'_6.jpg'}
  ];
  /***********Sonidos*****************/
    $scope.sonidos.Gabriella = [
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'1.mp3', descripcion: 'ANIVERSARIO'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'2.mp3', descripcion: 'CITA MÉDICA'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'3.mp3', descripcion: 'CUMPLEAÑOS'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'4.mp3', descripcion: 'DEPORTE'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'5.mp3', descripcion: 'EVENTO'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'6.mp3', descripcion: 'EXÁMEN'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'7.mp3', descripcion: 'JUEGOS'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'8.mp3', descripcion: 'MEDICINA'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'9.mp3', descripcion: 'REUNIÓN AMIGOS'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'10.mp3', descripcion: 'REUNIÓN'}
    ];
    $scope.sonidos.Georgina = [
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'1.mp3', descripcion: 'CITA MÉDICA'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'2.mp3', descripcion: 'DEPORTE'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'3.mp3', descripcion: 'JUEGOS'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'4.mp3', descripcion: 'MEDICINA'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'5.mp3', descripcion: 'REUNIÓN AMIGOS'}
    ];
    $scope.sonidos.Anmarie = [
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'1.mp3'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'2.mp3'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'3.mp3'}
    ];
    $scope.sonidos.Karen = [
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'1.mp3', descripcion: 'ANIVERSARIO'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'2.mp3', descripcion: 'CITA MÉDICA'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'3.mp3', descripcion: 'CUMPLEAÑOS'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'4.mp3', descripcion: 'DEPORTES'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'5.mp3', descripcion: 'DESPERTADOR'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'6.mp3', descripcion: 'EVENTO'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'7.mp3', descripcion: 'EXÁMEN'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'8.mp3', descripcion: 'JUEGOS'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'9.mp3', descripcion: 'MEDICINA'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'10.mp3', descripcion: 'REUNIÓN AMIGOS'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'11.mp3', descripcion: 'REUNIÓN'}
    ];
    $scope.sonidos.Grabiela = [
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'1.mp3', descripcion: 'ANIVERSARIO'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'2.mp3', descripcion: 'CUMPLEAÑOS'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'3.mp3', descripcion: 'DEPORTES'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'4.mp3', descripcion: 'DESPERTADOR'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'5.mp3', descripcion: 'EVENTO'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'6.mp3', descripcion: 'EXÁMEN'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'7.mp3', descripcion: 'JUEGOS'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'8.mp3', descripcion: 'MEDICINA'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'9.mp3', descripcion: 'REUNIÓN AMIGOS'},
        {link: 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'10.mp3', descripcion: 'REUNIÓN'}
    ];

  /*************Fin Sonidos****************/
  $scope.hiperGif = [
    {link: 'GIF/'+$scope.chicaSeleccion.Mychica.name+'1.gif'},
    {link: 'GIF/'+$scope.chicaSeleccion.Mychica.name+'2.gif'},
    {link: 'GIF/'+$scope.chicaSeleccion.Mychica.name+'3.gif'},
    {link: 'GIF/'+$scope.chicaSeleccion.Mychica.name+'4.gif'},
    {link: 'GIF/'+$scope.chicaSeleccion.Mychica.name+'5.gif'},
    {link: 'GIF/'+$scope.chicaSeleccion.Mychica.name+'6.gif'},
    {link: 'GIF/'+$scope.chicaSeleccion.Mychica.name+'7.gif'},
    {link: 'GIF/'+$scope.chicaSeleccion.Mychica.name+'8.gif'},
    {link: 'GIF/'+$scope.chicaSeleccion.Mychica.name+'9.gif'},
    {link: 'GIF/'+$scope.chicaSeleccion.Mychica.name+'10.gif'}
  ];
  $scope.preview_image = {};
  //$jq(".preview_image").append('<img src="'+'http://www.polarpilsen.com/'+$scope.tamanoSeleccion.Tamano.name+'/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_'+$scope.tamanoSeleccion.Tamano.cambio+'_'+$scope.chicaSeleccion.Mychica.name+'_1.jpg'+'" alt="" class="preview_img">');
  $scope.preview_image.model = 'http://www.polarpilsen.com/'+$scope.tamanoSeleccion.Tamano.name+'/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_'+$scope.tamanoSeleccion.Tamano.cambio+'_'+$scope.chicaSeleccion.Mychica.name+'_1.jpg';
  //$scope.preview_image.model = 'http://www.polarpilsen.com/'+$scope.tamanoSeleccion.Tamano.name+'/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_'+$scope.tamanoSeleccion.Tamano.cambio+'_'+$scope.chicaSeleccion.Mychica.name+'_1.jpg';
  $scope.cargaPreview = function(imagen, index){
    //console.log(imagen);
    if($scope.filtro == 1){
      // $jq(".preview_img").remove();
      // //$scope.preview_image.model = imagen;
      // $jq(".preview_image").append('<img src="'+imagen+'" alt="" class="preview_img">');
     // $scope.preview_image.model = imagen;
      $scope.preview_image.model = $scope.hiper[index].link;
    }else if($scope.filtro == 3){
      $scope.sonidos.link = imagen;
      $scope.descripcion_sonido = $scope.sonidos[$scope.chicaSeleccion.Mychica.name][index].descripcion;
      window.plugins.NativeAudio.stop( 'Reproductor' );
      window.plugins.NativeAudio.unload( 'Reproductor', function(){
        window.plugins.NativeAudio.preloadSimple( 'Reproductor', $scope.sonidos.link, function(msg){
        }, function(msg){
        });   
      }, function(){});
      //$jq(".preview_image").append('<img src="'+imagen+'" alt="" class="preview_img">');
    }
  }
  $scope.changeView = function(){
    if($scope.filtro == 1){
      for (var i = 0; i < 11; i++) {
        $scope.hiper[i] =  {link: 'http://www.polarpilsen.com/'+$scope.tamanoSeleccion.Tamano.name+'/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_'+$scope.tamanoSeleccion.Tamano.cambio+'_'+$scope.chicaSeleccion.Mychica.name+'_'+(parseInt(i)+1)+'.jpg'};
      };
       $jq("body .preview_img").remove();
      // $jq(".preview_image").append('<img src="http://www.polarpilsen.com/'+$scope.tamanoSeleccion.Tamano.name+'/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_'+$scope.tamanoSeleccion.Tamano.cambio+'_'+$scope.chicaSeleccion.Mychica.name+'_1.jpg" alt="" class="preview_img">');
      $scope.preview_image.model = 'http://www.polarpilsen.com/'+$scope.tamanoSeleccion.Tamano.name+'/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_'+$scope.tamanoSeleccion.Tamano.cambio+'_'+$scope.chicaSeleccion.Mychica.name+'_1.jpg';
      $ionicSlideBoxDelegate.update();
      $ionicSlideBoxDelegate.slide(0);
    }else if($scope.filtro == 2){
      $scope.preview_image.model = 'GIF/'+$scope.chicaSeleccion.Mychica.name+'1.gif';
      $jq("body .preview_img").remove();
      $jq(".preview_image").append('<img src="GIF/'+$scope.chicaSeleccion.Mychica.name+'1.gif" alt="" class="preview_img">');
      for (var i = 0; i < 11; i++) {
        $scope.hiper[i] =  {link: 'http://www.polarpilsen.com/'+$scope.tamanoSeleccion.Tamano.name+'/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_'+$scope.tamanoSeleccion.Tamano.cambio+'_'+$scope.chicaSeleccion.Mychica.name+'_'+(parseInt(i)+1)+'.jpg'};
      };
    }else if($scope.filtro == 3){
      if($scope.chicaSeleccion.Mychica.name == "Anmarie"){
        $jq("body .contenedor_recursos.sonidos").hide();
      }else{
        $jq("body .contenedor_recursos.sonidos").show();
      }
      $jq("body .preview_img").remove();
      if($scope.chicaSeleccion.Mychica.name !="Anmarie"){
        $scope.descripcion_sonido = $scope.sonidos[$scope.chicaSeleccion.Mychica.name][0].descripcion;
        $scope.sonidos.link = 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'1.mp3';
        window.plugins.NativeAudio.stop( 'Reproductor' );
        window.plugins.NativeAudio.unload( 'Reproductor', function(){
          window.plugins.NativeAudio.preloadSimple( 'Reproductor', $scope.sonidos.link, function(msg){
          }, function(msg){
          });   
        }, function(){});
      }
      $scope.preview_image.model = 'http://www.polarpilsen.com/'+$scope.tamanoSeleccion.Tamano.name+'/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_'+$scope.tamanoSeleccion.Tamano.cambio+'_'+$scope.chicaSeleccion.Mychica.name+'_1.jpg';
      //$jq(".preview_image").append('<img src="http://www.polarpilsen.com/'+$scope.tamanoSeleccion.Tamano.name+'/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_'+$scope.tamanoSeleccion.Tamano.cambio+'_'+$scope.chicaSeleccion.Mychica.name+'_1.jpg" alt="" class="preview_img">');
      for (var i = 0; i < 11; i++) {
        $scope.hiper[i] =  {link: 'http://www.polarpilsen.com/'+$scope.tamanoSeleccion.Tamano.name+'/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_'+$scope.tamanoSeleccion.Tamano.cambio+'_'+$scope.chicaSeleccion.Mychica.name+'_'+(parseInt(i)+1)+'.jpg'};
      };
    }

    //console.log($scope.chicaSeleccion.Mychica.name);
  }
    $jq('.filtro_bodega button').on({ 'touchstart' : function(){
            $jq("body .filtro_bodega button").each(function(e){
               $jq(this).removeClass("active");
               })
            $jq(this).addClass("active");
        }
    })
  $scope.share = function(){
    $jq(".preview_image").append('<div class="velo_share"></div>');
    if($scope.filtro==1){
      window.plugins.socialsharing.share(null, null, $scope.preview_image.model, null,function(){$jq(".preview_image .velo_share").remove();},function(){navigator.notification.alert('error', null, 'Share', 'OK');$jq(".preview_image .velo_share").remove();});
      analytics.sendEvent("Imagen Compartida", "Share", $scope.chicaSeleccion.Mychica.name, 1,function(){},function(){});
    }else if($scope.filtro==2){
      var link ='http://www.polarpilsen.com/'+$scope.preview_image.model;
      window.plugins.socialsharing.share(null, null,null,link,function(){$jq(".preview_image .velo_share").remove();},function(){navigator.notification.alert('error', null, 'Share', 'OK');$jq(".preview_image .velo_share").remove();});
      analytics.sendEvent("Gif Compartid", "Share", $scope.chicaSeleccion.Mychica.name, 1,function(){},function(){});
    }else if($scope.filtro==3){
      var link ='http://www.polarpilsen.com/'+$scope.sonidos.link;
      window.plugins.socialsharing.share(null, null, null, link,function(){$jq(".preview_image .velo_share").remove();},function(){navigator.notification.alert('error', null, 'Share', 'OK');$jq(".preview_image .velo_share").remove();});
      analytics.sendEvent("Sonido Compartido", "Share", $scope.chicaSeleccion.Mychica.name, 1,function(){},function(){});
    }
    
  }
  $scope.reproducir = function(){
    if($scope.chicaSeleccion.Mychica.name !="Anmarie"){
      window.plugins.NativeAudio.stop( 'Reproductor' );
      window.plugins.NativeAudio.unload( 'Reproductor', function(){
        window.plugins.NativeAudio.preloadSimple( 'Reproductor', $scope.sonidos.link, function(msg){
          window.plugins.NativeAudio.play( 'Reproductor' );
        }, function(msg){
        });
      }, function(){});
    }
  }
  $scope.filtroGif = function(){
      $jq(".preview_image_2").hide();
    $jq("body .contenedor_recursos").hide();
    $jq("body .home_bodega .titulo_seccion").hide();
    $jq("body .contenedor_recursos.sonidos").hide();
    $scope.filtro = 2;
    $jq(".reproducir_button").hide();
    $jq("body .preview_img").remove();
    $jq(".preview_image").append('<img src="GIF/'+$scope.chicaSeleccion.Mychica.name+'1.gif" alt="" class="preview_img">');
    $scope.preview_image.model = 'GIF/'+$scope.chicaSeleccion.Mychica.name+'1.gif';
    $scope.style1="";
  }
  $scope.filtroImagen = function(){
      $jq(".preview_image_2").show();
    $jq(".preview_img").attr("src","");
    $scope.preview_image.model = "";
    $jq("body .contenedor_recursos").show();
    $jq("body .home_bodega .titulo_seccion").show();
    $jq("body .contenedor_recursos.sonidos").hide();
    $scope.titulo_filtro ="Fotos";
    $scope.filtro = 1;
    $jq(".reproducir_button").hide();
    $jq("body .preview_img").remove();
   // $jq(".preview_image").append('<img src="http://www.polarpilsen.com/'+$scope.tamanoSeleccion.Tamano.name+'/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_'+$scope.tamanoSeleccion.Tamano.cambio+'_'+$scope.chicaSeleccion.Mychica.name+'_1.jpg" alt="" class="preview_img">');
    $scope.preview_image.model = 'http://www.polarpilsen.com/'+$scope.tamanoSeleccion.Tamano.name+'/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_'+$scope.tamanoSeleccion.Tamano.cambio+'_'+$scope.chicaSeleccion.Mychica.name+'_1.jpg';
    $scope.style1="";
  }
  $scope.filtroSonidos = function(){
      $jq(".preview_image_2").hide();
    $scope.descripcion_sonido = $scope.sonidos[$scope.chicaSeleccion.Mychica.name][0].descripcion;
    $jq("body .contenedor_recursos").hide();
    $jq("body .contenedor_recursos.sonidos").show();
    $jq("body .home_bodega .titulo_seccion").show();
    $scope.titulo_filtro ="Sonidos";
    $scope.filtro = 3;
    $jq(".reproducir_button").show();
    $jq("body .preview_img").remove();
    $jq(".preview_image").css("background","nayhib.png");
    $scope.style1="noback";
    $scope.sonidos.link = 'sonidos/'+$scope.chicaSeleccion.Mychica.name+'1.mp3';
    window.plugins.NativeAudio.preloadSimple( 'Reproductor', $scope.sonidos.link, function(msg){
    }, function(msg){
    });
    //$jq(".preview_image").append('<img src="http://www.polarpilsen.com/'+$scope.tamanoSeleccion.Tamano.name+'/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_'+$scope.tamanoSeleccion.Tamano.cambio+'_'+$scope.chicaSeleccion.Mychica.name+'_1.jpg" alt="" class="preview_img">');
    $scope.preview_image.model = 'http://www.polarpilsen.com/'+$scope.tamanoSeleccion.Tamano.name+'/'+$scope.chicaSeleccion.Mychica.name+'/FotosBodega_'+$scope.tamanoSeleccion.Tamano.cambio+'_'+$scope.chicaSeleccion.Mychica.name+'_1.jpg';
  }

  $scope.download= function(folder) {

    $jq(".preview_image").append('<div class="velo_share"></div>')
    if($scope.filtro == 1 || $scope.filtro == 2){
      var url =$scope.preview_image.model;
      var fileTransfer = new FileTransfer();
      var uri = encodeURI(url);
      var fileName = url.split(".");
      var date= new Date().getTime();
      var fileURL = folder + "ChicasPolarPilsen-"+date+"."+fileName[2];

      
      fileTransfer.download(
          uri,
          fileURL,
          function(entry) {
            $jq(".preview_image .velo_share").remove();
            navigator.notification.alert('Descarga', null, 'Bodega', 'OK');
            refreshMedia.refresh(fileURL);
          },
          function(error) {
            $jq(".preview_image .velo_share").remove();
            navigator.notification.alert('Error al descargar.', null, 'Bodega', 'OK');
          },
          true,
          {
              headers: {
                  "Authorization": "Basic dGVzdHVzZXJuYW1lOnRlc3RwYXNzd29yZA=="
              }
          }
      );
    }else if($scope.filtro == 3){
      var url =$scope.preview_image.model;
      var fileTransfer = new FileTransfer();
      var uri = encodeURI(url);
      var fileName = url.split(".");
      var date= new Date().getTime();
      var fileURL = folder +"ChicasPolarPilsen-"+ date+"."+fileName[1];
      fileTransfer.download(
          uri,
          fileURL,
          function(entry) {
            $jq(".preview_image .velo_share").remove();
            navigator.notification.alert('Descarga', null, 'Bodega', 'OK');
            refreshMedia.refresh(fileURL);
          },
          function(error) {
            $jq(".preview_image .velo_share").remove();
            navigator.notification.alert('Error al descargar.', null, 'Bodega', 'OK');
          },
          true,
          {
              headers: {
                  "Authorization": "Basic dGVzdHVzZXJuYW1lOnRlc3RwYXNzd29yZA=="
              }
          }
      );
    }
    analytics.sendEvent("Descarga Imagenes/GIF", "Click", $scope.chicaSeleccion.Mychica.name, 1,function(){},function(){});
  }
  /*$scope.downloadmp3= function(index,folder) {
    var url ='file:///android_asset/www/'+$scope.sonidos[index].link;
    var fileTransfer = new FileTransfer();
    var uri = encodeURI(url);
    var fileName = url.split(".");
    var date= new Date().getTime();
    var fileURL = folder + date+"."+fileName[1];
    //alert(fileURL);
    fileTransfer.download(
        uri,
        fileURL,
        function(entry) {
            alert("Descarga Completa");
            refreshMedia.refresh(fileURL);
            //console.log("download complete: " + entry.toURL());
        },
        function(error) {
            //alert("upload error code" + error.code);
            //console.log("download error source " + error.source);
            //console.log("download error target " + error.target);
            //console.log("upload error code" + error.code);
        },
        true,
        {
            headers: {
                "Authorization": "Basic dGVzdHVzZXJuYW1lOnRlc3RwYXNzd29yZA=="
            }
        }
    );
    analytics.sendEvent("Descarga MP3", "Click", $scope.chicaSeleccion.Mychica.name, 1);
  }*/
})
.controller('AlarmaHomeTabCtrl', function($scope, $state) {

    $scope.$on('$ionicView.enter', function(){
      $scope.tipoCalendarioSeleccion = window.localStorage.getItem("Calendario");
      $scope.chicasCalendarPremium = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
      $scope.chicasCalendarPremiumDate = new Date().getMonth();
      if($scope.tipoCalendarioSeleccion == 1){
        $scope.layout = 'CalendarioPremium/'+$scope.chicasCalendarPremium[$scope.chicasCalendarPremiumDate];
      }else if($scope.tipoCalendarioSeleccion == 2 || $scope.tipoCalendarioSeleccion == 3){
        $scope.chicaSeleccion =[
         // {name: "Karen"},
          {name: "Georgina"},
          {name: "Grabiela"},
          {name: "Gabriella"},
          {name: "Anmarie"},
          {name: "Grupal"}

        ];
        $scope.chicaSeleccionada = window.localStorage.getItem("chicaSeleccionada");
        $scope.fotoSeleccion = window.localStorage.getItem("fotoSeleccion");
        $scope.chicaSeleccion.Mychica =  $scope.chicaSeleccion[$scope.chicaSeleccionada];
        $scope.layout = 'DetalleChicaEscogida/'+$scope.chicaSeleccion.Mychica.name+'/EscogeChica_'+$scope.chicaSeleccion.Mychica.name+'_'+(parseInt($scope.fotoSeleccion)+1);
      }
      $scope.TituloAlarmaSuena = window.localStorage.getItem("tituloAlarma");
      $scope.HoraAlarmaSuena = window.localStorage.getItem("HoraAlarma");
      $scope.MinutosAlarmaSuena = window.localStorage.getItem("MinutosAlarma");
      $scope.FormatoAlarmaSuena = window.localStorage.getItem("FormatoAlarma");
    })
    $scope.detenerAlarma = function(){
      window.plugins.NativeAudio.stop( 'Grabiela' );
      window.plugins.NativeAudio.stop( 'Karen' );
      window.plugins.NativeAudio.unload( 'Grabiela' );
      window.plugins.NativeAudio.unload( 'Karen' );
      $state.go('tabs.despertador');
    }

})
.controller('loaderCTRL', function($scope, $state) {
  $jq("body .loader_ini").hide();
  $scope.registrado = window.localStorage.getItem("registrado");
  /*if($scope.registrado == 1){
    db.transaction(queryDateHome, function(err){navigator.notification.alert(err, null, 'Share', 'OK');});
  }
  
  function queryDateHome(tx){
    tx.executeSql ('SELECT * FROM DEMO', [], successDateHome, function(err){navigator.notification.alert(err, null, 'Share', 'OK');});
  }

  function successDateHome(tx, results) {
    events = {};
    for (var i = 0; i < results.rows.length; i++) {
        //console.log ("ID=" + results.rows.item(i).ID+" Fecha Inicio= "+results.rows.item(i).mes_inicio+"-"+results.rows.item(i).dia_inicio+"-"+results.rows.item(i).ano_inicio);
        var hora_inicio =results.rows.item(i).hora_inicio;
        var minutos_inicio = results.rows.item(i).minutos_inicio;
        var hora_fin = results.rows.item(i).hora_fin;
        var minutos_fin = results.rows.item(i).minutos_fin;
        var mes_inicio = results.rows.item(i).mes_inicio;
        var dia_inicio = results.rows.item(i).dia_inicio;
        var ano_inicio = results.rows.item(i).ano_inicio;
        var ano_fin = results.rows.item(i).ano_fin;
        var dia_fin = results.rows.item(i).dia_fin;
        var mes_fin = results.rows.item(i).mes_fin;
        var startDate = (mes_inicio < 10 ? '0' + (parseInt(mes_inicio)+1 ) : (parseInt(mes_inicio)+1)) + "-" + ((dia_inicio) < 10 ? '0' + (dia_inicio) : (dia_inicio)) + "-" + ano_inicio;
        var endDate = mes_fin+"-"+dia_fin+"-"+ano_fin;
        var dtstart_calendario = hora_inicio+":"+minutos_inicio;
        var dtend_calendario = hora_fin+":"+minutos_fin;
        var title = results.rows.item(i).titulo;
        var ID=results.rows.item(i).ID;
        var repeticion = results.rows.item(i).repeticion;
        //console.log ("ID=" + results.rows.item(i).ID+" Fecha Inicio= "+startDate);
        $scope.ID = ID + 1;
        if((startDate in events)){
            events[startDate].push({content: title, startTime: dtstart_calendario, endTime: dtend_calendario, note: ID, recordatorio:repeticion});
        }else{
            events[startDate]=[{content: title, startTime: dtstart_calendario, endTime: dtend_calendario, note: ID, recordatorio:repeticion}];
        }

    };
    $state.go('tabs.home');
  }
  $scope.$on('$ionicView.enter', function(){
    $scope.infoEvent = window.localStorage.getItem("event_CLICK");
    if($scope.infoEvent == 1){
      $state.go('tabs.home');
    }
  })*/

})
.controller('settingsHomeTabCtrl', function($scope, $state) {
  analytics.sendAppView("/Settings", function(){},function(){});
  $scope.goTo = function(){
    window.localStorage.setItem("edicion", 1);
    $state.go('tipoCalendario');
  }
})
.controller('calendario_mesesCtrl', function($scope, $state) {
}).directive('myclick', function() {
                          return function(scope, element, attrs) {
                          element.bind('touchstart click', function(event) {
                                       //console.log(event);
                                       event.preventDefault();
                                       event.stopPropagation();
                                       if (event.type === 'touchstart') {
                                       scope.$apply(attrs['myclick']);
                                       }
                                       });
                          };
                          });



function formatDate(date) {
    var d = new Date(date);
    var hh = d.getHours();
    var m = d.getMinutes();
    var s = d.getSeconds();
    var dd = "am";
    var h = hh;
    if (h >= 12) {
        h = hh-12;
        dd = "pm";
    }
    if (h == 0) {
        h = 12;
    }
    m = m<10?"0"+m:m;

    s = s<10?"0"+s:s;

    /* if you want 2 digit hours:
    h = h<10?"0"+h:h; */

    var pattern = new RegExp("0?"+hh+":"+m+":"+s);

    var replacement = h+":"+m;
    /* if you want to add seconds
    replacement += ":"+s;  */
    replacement += " "+dd;    

    return replacement;
}
function formatDateUTC(date) {
    var d = new Date(date);
    var hh = d.getUTCHours();
    var m = d.getUTCMinutes();
    var s = d.getSeconds();
    var dd = "am";
    var h = hh;
    if (h >= 12) {
        h = hh-12;
        dd = "pm";
    }
    if (h == 0) {
        h = 12;
    }
    m = m<10?"0"+m:m;

    s = s<10?"0"+s:s;

    /* if you want 2 digit hours:
    h = h<10?"0"+h:h; */

    var pattern = new RegExp("0?"+hh+":"+m+":"+s);

    var replacement = h+":"+m;
    /* if you want to add seconds
    replacement += ":"+s;  */
    replacement += " "+dd;    

    return replacement;
}
function formatHour(date) {
    var d = new Date(date);
    var hh = d.getHours();
    var dd = "am";
    var h = hh;
    if (h >= 12) {
        h = hh-12;
        dd = "pm";
    }
    if (h == 0) {
        h = 12;
    }

    var replacement = h;

    return replacement;
}

